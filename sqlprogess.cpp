#include "sqlprogess.h"
#include "DataBaseConnect.h"
#include <QDebug>
#include <QtCore>
bool SqlProgess::createConnect()
{

    QTextCodec *codec = QTextCodec::codecForName( "UTF-8" );
    QTextCodec::setCodecForLocale( codec );
    //m_db = QSqlDatabase::addDatabase("QMYSQL","127.0.0.1@3306");
    m_db= QSqlDatabase::addDatabase("QSQLITE");//本地数据库
    //m_db.setHostName("localhost");
    // 将数据库存储在应用程序二进制文件夹下
    // 先确认文件是否存在
    QString databasePath = QApplication::applicationDirPath()+"//Data//";
    QDir dir(databasePath);
    // 新建文件夹
    if(dir.exists()==false){
        dir.mkdir(databasePath);
    }
    // 打开数据库文件
    //m_db.setDatabaseName("datasystem");       //这里输入你的数据库名
    m_db.setDatabaseName(databasePath+"dataPackage.db");
    // 添加数据库链接用户名模块
    Ui_Dialog uDialog;
    QDialog *w = new QDialog();
    uDialog.setupUi(w);
    QString username,password;
    if(w->exec() == QDialog::Accepted){
        uDialog.GetInputLoginInfo(&username,&password);
    }
    else{
        QMessageBox::critical(0, QObject::tr("登录失败"),"用户取消操作！ ", QMessageBox::Cancel);
        return false;
    }
    m_db.setUserName(username);
    m_db.setPassword(password);

    //直接写死用户名密码，而且是弱密码太危险了
    //m_db.setUserName("root");
    //m_db.setPassword("root");   //这里输入你的密码
    if ( !m_db.open() )
    {

        QMessageBox::critical(0, QObject::tr("无法打开数据库"),"无法创建数据库连接！ ", QMessageBox::Cancel);
        return false;
    }
    else
    {
        return true;
    }
}

SqlProgess::SqlProgess()
{

}

SqlProgess::~SqlProgess()
{
    if( m_db.isOpen() )
    {
        m_db.close();
    }
}
void SqlProgess::InsertData( QString  TableName, tagData dJD, tagData dGC, tagIMU dBH)
{
    if( !m_db.isOpen() )
    {
        QMessageBox::critical(0, QObject::tr("无法打开数据库"),"无法创建数据库连接！ ", QMessageBox::Cancel);
        return ;
    }
    QString strSql= QString("INSERT OR IGNORE INTO `%1` (`TimeData`, `JDData`, `GCData`,`BHData` ) VALUES (:TimeData, :JDData, :GCData, :BHData);" ).arg( TableName );

    qDebug() << strSql;
    QByteArray qbaJDData;
    qbaJDData.resize(static_cast<int>(104));

    QByteArray qbaGCData;
    qbaGCData.resize(static_cast<int>(104));

    QByteArray qbaADDIMU;
    qbaADDIMU.resize(static_cast<int>(19));


    QSqlQuery sqlQuery;
    sqlQuery.prepare( strSql );
    QString strime = QDateTime::currentDateTime().toString(  "yyyy-MM-dd hh:mm:ss");

    sqlQuery.bindValue( ":TimeData" , strime  );
    memcpy( qbaJDData.data() , &dJD , 104 );
    sqlQuery.bindValue( ":JDData" ,qbaJDData );

    memcpy( qbaGCData.data() , &dGC , 104 );
    sqlQuery.bindValue( ":GCData" ,qbaGCData );

    memcpy( qbaADDIMU.data() , &dBH , 19 );
    sqlQuery.bindValue( ":BHData" ,qbaADDIMU );
    if(!sqlQuery.exec())
      {
          qDebug() << sqlQuery.lastError().text();
      }

}

bool SqlProgess::CreateTable(QString strTableName)
{
    QSqlQuery query(m_db);
    QString   strSql;
    /*
    strSql = QString("CREATE TABLE IF NOT EXISTS `%1`("
                     "`TimeData` DATETIME NOT NULL,"
                     "`JDData` BLOB(104) DEFAULT NULL,"   "`GCData` BLOB(104) DEFAULT NULL,"    "`BHData` BLOB(19) DEFAULT NULL,"
                     "PRIMARY KEY (`TimeData`)"
                     ")ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED;"
                     ).arg( strTableName );
    */
    strSql = QString("CREATE TABLE IF NOT EXISTS `%1`("
                     "`TimeData` DATETIME NOT NULL,"
                     "`JDData` BLOB(104) DEFAULT NULL,"   "`GCData` BLOB(104) DEFAULT NULL,"    "`BHData` BLOB(19) DEFAULT NULL,"
                     "PRIMARY KEY (`TimeData`)"
                     ");"
                     ).arg( strTableName );
    if( !query.exec( strSql ) )
        {
           return false;
        }
    return true;
}

void SqlProgess::ReadSearchData( QString TableName , QDateTime StartD, QDateTime endD )
{
    m_VecJD.clear();
    m_VecGC.clear();
    m_VecBH.clear();

    QSqlQuery SqlQuery( m_db );

    QByteArray qbaADData;
    qbaADData.resize(static_cast<int>(104));

    QByteArray qbaADDIMU;
    qbaADData.resize(static_cast<int>(19));

    QString qsSql = QString("select * from %1 where TimeData >= '%2' and TimeData <= '%3'").arg( TableName ).arg( StartD.toString( "yyyy-MM-dd hh:mm:ss"  ) ).arg( endD.toString( "yyyy-MM-dd hh:mm:ss"  ) );

    if( !SqlQuery.exec(qsSql))
    {
        return;
    }
    while( SqlQuery.next() )
    {
      tagData TData; tagIMU tIMU;
      QDateTime  dateT = SqlQuery.value(0).toDateTime();
      m_DateT.append( dateT );
      QByteArray  byte = SqlQuery.value(1).toByteArray();
      memcpy( &TData ,byte.data() , 104  );
      m_VecJD.append( TData );

      byte = SqlQuery.value(2).toByteArray();
      memcpy( &TData ,byte.data() , 104  );
      m_VecGC.append( TData );

      byte = SqlQuery.value(3).toByteArray();
      memcpy( &tIMU ,byte.data() , 19  );
      m_VecBH.append( tIMU );
    }

}
