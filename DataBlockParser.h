#pragma once
#ifndef DATA_PARSER
#define DATA_PARSER
// define the 40bit data block struct
// ChannelBlockData : the struct itself
// PChannelBlockData : A pointer point to an array of ChannelBlockData.
typedef struct {
    bool startHeader;
    bool isTemperature;
    unsigned char numChannel;
    int channelData;
    bool checkSum;
    bool endTail;
}ChannelBlockData, * PChannelBlockData;

//Parser Class
class DataBlockParser
{
private:
    // pointer to ChannelBlockArray
    PChannelBlockData pchannelBlock = nullptr;
    // frame header magic words read from data.
    int frameHeader = 0;
    // timeStamp read from data.
    int timeStamp = 0;
    // extern data read from data block.
    unsigned char* externData = nullptr;
    // CRC16CheckSum data read from data block.
    unsigned short CRC16CheckSum = 0;
    // frame tail magic words read from data.
    int frameTail = 0;
public:
    // class constructed function. Do memory alloc.
    DataBlockParser();
    // class destructor function. Do memory free.
    ~DataBlockParser();
    // Data parser.
    // pBlock: a pointer to data block
    // blockSize: Read length.
    // isLittleEndian: Operation 40bit data block as is ordered in little endian.
    bool Parser(const unsigned char* pBlock, int blockSize, bool isLittleEndian);
    // user interface to read data from parser.
    // blockIndex: 40bit data block index.
    // channel: channel read from datablock.
    // channelData: data read from datablock.
    // isTemp: true if channel is temperature channel.
    bool GetChannelINFO(int blockIndex, unsigned char* const channel, int* const channelData, bool* isTemp);
    // return frame header.
    int GetFrameHeader();
    // return time data from data block.
    int GetTimeStamp();
    // return extern data from data block.
    bool GetExternData(unsigned char* const dstBlock, int copySize);
    // return frame tail.
    int GetFrameTail();
    // get 40bit block struct data
    bool GetBlockStruct(int blockIndex, const PChannelBlockData blockpointer);
    // all these function return false if error occured.
};
#endif
