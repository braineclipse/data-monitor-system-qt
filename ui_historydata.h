/********************************************************************************
** Form generated from reading UI file 'historydatacQfukg.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef HISTORYDATACQFUKG_H
#define HISTORYDATACQFUKG_H

#include <QtCore/QDate>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

QT_BEGIN_NAMESPACE

class Ui_HistoryData
{
public:
    QGridLayout *gridLayout_7;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QLabel *label;
    QDateTimeEdit *dateTimeEdit;
    QDateTimeEdit *dateTimeEdit_2;
    QLabel *label_2;
    QGroupBox *groupBox_2;
    QPushButton *PBResearch;
    QPushButton *PBPlay;
    QGroupBox *groupBox_3;
    QTextEdit *textEdit;
    QTabWidget *tabDataH;
    QWidget *tabJDH;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QCustomPlot *JDHGroTempature1;
    QCustomPlot *JDHGroTempature2;
    QHBoxLayout *horizontalLayout_2;
    QCustomPlot *HGCGroTempature1;
    QCustomPlot *HGCGroTempature2;
    QWidget *tabGCH;
    QGridLayout *gridLayout_6;
    QHBoxLayout *horizontalLayout_3;
    QCustomPlot *HGCStrain1;
    QCustomPlot *HGCStrain2;
    QHBoxLayout *horizontalLayout_4;
    QCustomPlot *JDHStrain1;
    QCustomPlot *JDHStrain2;
    QWidget *tabBHH;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QCustomPlot *HRoteX;
    QCustomPlot *HRoteY;
    QCustomPlot *HRoteZ;

    void setupUi(QDialog *HistoryData)
    {
        if (HistoryData->objectName().isEmpty())
            HistoryData->setObjectName(QString::fromUtf8("HistoryData"));
        HistoryData->resize(1136, 738);
        gridLayout_7 = new QGridLayout(HistoryData);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(HistoryData);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMaximumSize(QSize(16777215, 166));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(4, 20, 91, 20));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        dateTimeEdit = new QDateTimeEdit(groupBox);
        dateTimeEdit->setObjectName(QString::fromUtf8("dateTimeEdit"));
        dateTimeEdit->setGeometry(QRect(2, 50, 204, 31));
        dateTimeEdit->setFont(font);
        dateTimeEdit->setDateTime(QDateTime(QDate(2021, 10, 8), QTime(0, 0, 0)));
        dateTimeEdit->setDate(QDate(2021, 10, 8));
        dateTimeEdit_2 = new QDateTimeEdit(groupBox);
        dateTimeEdit_2->setObjectName(QString::fromUtf8("dateTimeEdit_2"));
        dateTimeEdit_2->setGeometry(QRect(3, 120, 203, 31));
        dateTimeEdit_2->setFont(font);
        dateTimeEdit_2->setDateTime(QDateTime(QDate(2021, 10, 10), QTime(0, 0, 0)));
        dateTimeEdit_2->setDate(QDate(2021, 10, 10));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(3, 90, 91, 20));
        label_2->setFont(font);

        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(HistoryData);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(277, 0));
        groupBox_2->setMaximumSize(QSize(301, 55));
        PBResearch = new QPushButton(groupBox_2);
        PBResearch->setObjectName(QString::fromUtf8("PBResearch"));
        PBResearch->setGeometry(QRect(10, 10, 75, 39));
        PBResearch->setMinimumSize(QSize(0, 39));
        PBResearch->setMaximumSize(QSize(16777215, 75));
        PBPlay = new QPushButton(groupBox_2);
        PBPlay->setObjectName(QString::fromUtf8("PBPlay"));
        PBPlay->setGeometry(QRect(130, 10, 75, 37));
        PBPlay->setMinimumSize(QSize(0, 0));
        PBPlay->setMaximumSize(QSize(16777215, 37));

        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(HistoryData);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(249, 327));
        textEdit = new QTextEdit(groupBox_3);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(1, 7, 273, 473));
        textEdit->setMinimumSize(QSize(273, 473));
        textEdit->setMaximumSize(QSize(16777215, 16777215));
        textEdit->setSizeIncrement(QSize(273, 0));
        textEdit->setFrameShape(QFrame::NoFrame);

        verticalLayout->addWidget(groupBox_3);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        tabDataH = new QTabWidget(HistoryData);
        tabDataH->setObjectName(QString::fromUtf8("tabDataH"));
        tabDataH->setMinimumSize(QSize(812, 0));
        QFont font1;
        font1.setPointSize(14);
        tabDataH->setFont(font1);
        tabDataH->setTabPosition(QTabWidget::East);
        tabJDH = new QWidget();
        tabJDH->setObjectName(QString::fromUtf8("tabJDH"));
        gridLayout_5 = new QGridLayout(tabJDH);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        JDHGroTempature1 = new QCustomPlot(tabJDH);
        JDHGroTempature1->setObjectName(QString::fromUtf8("JDHGroTempature1"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(JDHGroTempature1->sizePolicy().hasHeightForWidth());
        JDHGroTempature1->setSizePolicy(sizePolicy);
        JDHGroTempature1->setMinimumSize(QSize(300, 300));

        horizontalLayout->addWidget(JDHGroTempature1);

        JDHGroTempature2 = new QCustomPlot(tabJDH);
        JDHGroTempature2->setObjectName(QString::fromUtf8("JDHGroTempature2"));
        sizePolicy.setHeightForWidth(JDHGroTempature2->sizePolicy().hasHeightForWidth());
        JDHGroTempature2->setSizePolicy(sizePolicy);
        JDHGroTempature2->setMinimumSize(QSize(300, 300));

        horizontalLayout->addWidget(JDHGroTempature2);


        gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        HGCGroTempature1 = new QCustomPlot(tabJDH);
        HGCGroTempature1->setObjectName(QString::fromUtf8("HGCGroTempature1"));
        sizePolicy.setHeightForWidth(HGCGroTempature1->sizePolicy().hasHeightForWidth());
        HGCGroTempature1->setSizePolicy(sizePolicy);
        HGCGroTempature1->setMinimumSize(QSize(300, 300));

        horizontalLayout_2->addWidget(HGCGroTempature1);

        HGCGroTempature2 = new QCustomPlot(tabJDH);
        HGCGroTempature2->setObjectName(QString::fromUtf8("HGCGroTempature2"));
        sizePolicy.setHeightForWidth(HGCGroTempature2->sizePolicy().hasHeightForWidth());
        HGCGroTempature2->setSizePolicy(sizePolicy);
        HGCGroTempature2->setMinimumSize(QSize(300, 300));

        horizontalLayout_2->addWidget(HGCGroTempature2);


        gridLayout_2->addLayout(horizontalLayout_2, 1, 0, 1, 1);


        gridLayout_5->addLayout(gridLayout_2, 0, 0, 1, 1);

        tabDataH->addTab(tabJDH, QString());
        tabGCH = new QWidget();
        tabGCH->setObjectName(QString::fromUtf8("tabGCH"));
        sizePolicy.setHeightForWidth(tabGCH->sizePolicy().hasHeightForWidth());
        tabGCH->setSizePolicy(sizePolicy);
        gridLayout_6 = new QGridLayout(tabGCH);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        HGCStrain1 = new QCustomPlot(tabGCH);
        HGCStrain1->setObjectName(QString::fromUtf8("HGCStrain1"));
        sizePolicy.setHeightForWidth(HGCStrain1->sizePolicy().hasHeightForWidth());
        HGCStrain1->setSizePolicy(sizePolicy);
        HGCStrain1->setMinimumSize(QSize(300, 300));

        horizontalLayout_3->addWidget(HGCStrain1);

        HGCStrain2 = new QCustomPlot(tabGCH);
        HGCStrain2->setObjectName(QString::fromUtf8("HGCStrain2"));
        sizePolicy.setHeightForWidth(HGCStrain2->sizePolicy().hasHeightForWidth());
        HGCStrain2->setSizePolicy(sizePolicy);
        HGCStrain2->setMinimumSize(QSize(300, 300));

        horizontalLayout_3->addWidget(HGCStrain2);


        gridLayout_6->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        JDHStrain1 = new QCustomPlot(tabGCH);
        JDHStrain1->setObjectName(QString::fromUtf8("JDHStrain1"));
        sizePolicy.setHeightForWidth(JDHStrain1->sizePolicy().hasHeightForWidth());
        JDHStrain1->setSizePolicy(sizePolicy);
        JDHStrain1->setMinimumSize(QSize(300, 300));

        horizontalLayout_4->addWidget(JDHStrain1);

        JDHStrain2 = new QCustomPlot(tabGCH);
        JDHStrain2->setObjectName(QString::fromUtf8("JDHStrain2"));
        sizePolicy.setHeightForWidth(JDHStrain2->sizePolicy().hasHeightForWidth());
        JDHStrain2->setSizePolicy(sizePolicy);
        JDHStrain2->setMinimumSize(QSize(300, 300));

        horizontalLayout_4->addWidget(JDHStrain2);


        gridLayout_6->addLayout(horizontalLayout_4, 1, 0, 1, 1);

        tabDataH->addTab(tabGCH, QString());
        tabBHH = new QWidget();
        tabBHH->setObjectName(QString::fromUtf8("tabBHH"));
        sizePolicy.setHeightForWidth(tabBHH->sizePolicy().hasHeightForWidth());
        tabBHH->setSizePolicy(sizePolicy);
        gridLayout_3 = new QGridLayout(tabBHH);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        HRoteX = new QCustomPlot(tabBHH);
        HRoteX->setObjectName(QString::fromUtf8("HRoteX"));
        sizePolicy.setHeightForWidth(HRoteX->sizePolicy().hasHeightForWidth());
        HRoteX->setSizePolicy(sizePolicy);
        HRoteX->setMinimumSize(QSize(300, 300));

        horizontalLayout_5->addWidget(HRoteX);

        HRoteY = new QCustomPlot(tabBHH);
        HRoteY->setObjectName(QString::fromUtf8("HRoteY"));
        sizePolicy.setHeightForWidth(HRoteY->sizePolicy().hasHeightForWidth());
        HRoteY->setSizePolicy(sizePolicy);
        HRoteY->setMinimumSize(QSize(300, 300));

        horizontalLayout_5->addWidget(HRoteY);


        gridLayout_4->addLayout(horizontalLayout_5, 0, 0, 1, 1);

        HRoteZ = new QCustomPlot(tabBHH);
        HRoteZ->setObjectName(QString::fromUtf8("HRoteZ"));
        sizePolicy.setHeightForWidth(HRoteZ->sizePolicy().hasHeightForWidth());
        HRoteZ->setSizePolicy(sizePolicy);
        HRoteZ->setMinimumSize(QSize(300, 300));

        gridLayout_4->addWidget(HRoteZ, 1, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_4, 0, 0, 1, 1);

        tabDataH->addTab(tabBHH, QString());

        gridLayout->addWidget(tabDataH, 0, 1, 1, 1);


        gridLayout_7->addLayout(gridLayout, 0, 0, 1, 1);


        retranslateUi(HistoryData);

        tabDataH->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(HistoryData);
    } // setupUi

    void retranslateUi(QDialog *HistoryData)
    {
        HistoryData->setWindowTitle(QCoreApplication::translate("HistoryData", "\345\216\206\345\217\262\346\225\260\346\215\256\347\252\227\345\217\243", nullptr));
        groupBox->setTitle(QCoreApplication::translate("HistoryData", "\347\255\233\351\200\211\346\235\241\344\273\266", nullptr));
        label->setText(QCoreApplication::translate("HistoryData", "\345\274\200\345\247\213\346\227\266\351\227\264\357\274\232", nullptr));
        dateTimeEdit->setDisplayFormat(QCoreApplication::translate("HistoryData", "yyyy-MM-dd HH:mm:ss", nullptr));
        dateTimeEdit_2->setDisplayFormat(QCoreApplication::translate("HistoryData", "yyyy-MM-dd HH:mm:ss", nullptr));
        label_2->setText(QCoreApplication::translate("HistoryData", " \347\273\223\346\235\237\346\227\266\351\227\264\357\274\232", nullptr));
        groupBox_2->setTitle(QString());
        PBResearch->setText(QCoreApplication::translate("HistoryData", "   \345\233\236\346\224\276", nullptr));
        PBPlay->setText(QCoreApplication::translate("HistoryData", "\345\201\234\346\255\242", nullptr));
        groupBox_3->setTitle(QString());
        textEdit->setHtml(QCoreApplication::translate("HistoryData", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'SimSun'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">    \345\216\206\345\217\262\346\225\260\346\215\256\346\237\245\350\257\242\357\274\214\347\224\250\346\210\267\350\276\223\345\205\245\350\265\267\345\247\213\346\237\245\350\257\242\346\227\266\351\227\264\345\222\214\347\273\223\346\235\237\346\237\245\350\257\242\346\227\266\351\227\264\357\274\214\347\202\271\345\207\273\342\200\234\346\237\245\350\257\242\342\200\235\346\214\211\351\222\256\346\227\242\345\217\257\344\273\245\346\237\245\350\257\242\346\211\200\350\246\201\346\237\245\350\257\242\347\232\204\346\225\260\346\215"
                        "\256\343\200\202\347\202\271\345\207\273\342\200\234\345\257\274\345\207\272\342\200\235\346\214\211\351\222\256\357\274\214\345\217\257\344\273\245\345\257\274\345\207\272\347\224\250\346\210\267\346\237\245\350\257\242\347\232\204\346\225\260\346\215\256\344\277\241\346\201\257\343\200\202</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">    Historical data query, the user enters the start query time and the end query time, and clicks the &quot;query&quot; button to query the data to be queried. Click the &quot;Export&quot; button to export the data information of the user query.</span></p></body></html>", nullptr));
        tabDataH->setTabText(tabDataH->indexOf(tabJDH), QCoreApplication::translate("HistoryData", " \346\225\260\346\215\2561", nullptr));
        tabDataH->setTabText(tabDataH->indexOf(tabGCH), QCoreApplication::translate("HistoryData", "\346\225\260\346\215\2562", nullptr));
        tabDataH->setTabText(tabDataH->indexOf(tabBHH), QCoreApplication::translate("HistoryData", "\346\225\260\346\215\2563", nullptr));
    } // retranslateUi

};

namespace Ui {
    class HistoryData: public Ui_HistoryData {};
} // namespace Ui

QT_END_NAMESPACE

#endif // HISTORYDATACQFUKG_H
