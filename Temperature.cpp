#include "Temperature.h"
#include "DataBlockParser.h"
Temperature::Temperature(QObject *parent) : QObject(parent)
{
    m_ReceiveBytes.clear();
    m_iK = 1e6;
}
void Temperature::InitParam(const QString &strCom, const int &iBaud)
{
    m_strCom  = strCom;
    m_iBaud   = iBaud;

    //设置串口参数
    m_SerialPort.setPortName( m_strCom );
    m_SerialPort.setBaudRate( m_iBaud );
    m_SerialPort.setDataBits( QSerialPort::Data8 );
    m_SerialPort.setParity( QSerialPort::NoParity );
    m_SerialPort.setStopBits( QSerialPort::OneStop );
    m_SerialPort.setFlowControl( QSerialPort::NoFlowControl );

    //打开串口
    if( m_SerialPort.isOpen() )
    {
        m_SerialPort.clear();
        m_SerialPort.close();
    }
    m_SerialPort.open( QIODevice::ReadWrite );
}

void Temperature::ParamTemperature(QByteArray byte)
{
    int DataLength = 214 ;
    QString startHead("fffefeff");
    QByteArray arrHead =  QByteArray::fromHex( startHead.toUtf8() );
    tagData cData ;
    //only need init it when first run.
    //the memory once alloced will be reused until dataParesr.~DataBlockParser() function is called.
    DataBlockParser dataParser = DataBlockParser();
    // These vars are nolonger needed.
    //int iH = 0;
    //int iL = 0;
    //int iV = 0;
    while( m_ReceiveBytes.size() >= DataLength )
    {
        int iPos = m_ReceiveBytes.indexOf( arrHead );
        if( iPos < 0 )      // did not find head
        {
           m_ReceiveBytes.clear();
           return;
        }
        else if( iPos > 0 )     // has data before head
        {
           m_ReceiveBytes.remove( 0, iPos );   // remove data before head
        }
        // add a parser for data reading;
        while( m_ReceiveBytes.size() >= DataLength )
        {
            dataParser.Parser((uchar * )m_ReceiveBytes.data(),214,true);
            cData.m_uiTime = dataParser.GetTimeStamp();
            uchar channel = 0;
            int data = 0;
            bool isTemp;
            dataParser.GetChannelINFO(0,&channel,&data,&isTemp);
            cData.m_cSstate1 = channel;
            cData.m_fstrain1 = data /10/ 3.258;

            dataParser.GetChannelINFO(1,&channel,&data,&isTemp);
            cData.m_cSstate2 = channel;
            cData.m_fstrain2 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(2,&channel,&data,&isTemp);
            cData.m_cSstate3 = channel;
            cData.m_fstrain3 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(3,&channel,&data,&isTemp);
            cData.m_cSstate4 = channel;
            cData.m_fstrain4 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(4,&channel,&data,&isTemp);
            cData.m_cSstate5 = channel;
            cData.m_fstrain5 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(5,&channel,&data,&isTemp);
            cData.m_cSstate6 = channel;
            cData.m_fstrain6 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(6,&channel,&data,&isTemp);
            cData.m_cSstate7 = channel;
            cData.m_fstrain7 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(7,&channel,&data,&isTemp);
            cData.m_cSstate8 = channel;
            cData.m_fstrain8 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(8,&channel,&data,&isTemp);
            cData.m_cSstate9 = channel;
            cData.m_fstrain9 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(9,&channel,&data,&isTemp);
            cData.m_cSstate10 = channel;
            cData.m_fstrain10 = data /10.0/ 3.258;

            dataParser.GetChannelINFO(10,&channel,&data,&isTemp);
            cData.m_cState1 = channel;
            cData.m_fGeothermal1 = data /1e3*0.017;

            dataParser.GetChannelINFO(11,&channel,&data,&isTemp);
            cData.m_cState2 = channel;
            cData.m_fGeothermal2 = data /1e4*0.017;

            dataParser.GetChannelINFO(12,&channel,&data,&isTemp);
            cData.m_cState3 = channel;
            cData.m_fGeothermal3 = data /1e4*0.017;

            dataParser.GetChannelINFO(13,&channel,&data,&isTemp);
            cData.m_cState4 = channel;
            cData.m_fGeothermal4 = data /1e4*0.017;

            dataParser.GetChannelINFO(14,&channel,&data,&isTemp);
            cData.m_cState5 = channel;
            cData.m_fGeothermal5 = data /1e4*0.017;

            dataParser.GetChannelINFO(15,&channel,&data,&isTemp);
            cData.m_cState6 = channel;
            cData.m_fGeothermal6 = data /1e4*0.017 + 15818.0;

            dataParser.GetChannelINFO(16,&channel,&data,&isTemp);
            cData.m_cState7 = channel;
            cData.m_fGeothermal7 = data /1e4*0.017;

            dataParser.GetChannelINFO(17,&channel,&data,&isTemp);
            cData.m_cState8 = channel;
            cData.m_fGeothermal8 = data /1e4*0.017;

            dataParser.GetChannelINFO(18,&channel,&data,&isTemp);
            cData.m_cState9 = channel;
            cData.m_fGeothermal9 = data /1e4*0.017;

            dataParser.GetChannelINFO(19,&channel,&data,&isTemp);
            cData.m_cState10 = channel;
            cData.m_fGeothermal10 = data /1e4*0.017;
    /* old no-work codes
    int iH = 0;
    int iL = 0;
    int iV = 0;
    float i_Temp = 1e6;
    while( m_ReceiveBytes.size() >= DataLength )
    {
        int iPos = m_ReceiveBytes.indexOf( arrHead );
        if( iPos < 0 )      // did not find head
        {
           m_ReceiveBytes.clear();
           return;
        }
        else if( iPos > 0 )     // has data before head
        {
           m_ReceiveBytes.remove( 0, iPos );   // remove data before head
        }

        while( m_ReceiveBytes.size() >= DataLength )
        {
            uchar* pData = (uchar*)m_ReceiveBytes.data();
           // memcpy( &cData , pData + 4 , sizeof ( tagData  ) );
            memcpy( &cData.m_uiTime, pData + 4 , 4 );
            cData.m_cSstate1 = ( ( pData[8] & 0x3c ) >> 2 );
            iH = int(( pData[8] & 0x03   ) << 30 );
            memcpy( &iL , &pData[9] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain1 = iV / m_iK ;

            cData.m_cSstate2 = ( ( pData[13] & 0x3c ) >> 2 );
            iH = int(( pData[13] & 0x03   ) << 30 );
            memcpy( &iL , &pData[14] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain2 = iV / m_iK ;

            cData.m_cSstate3 = ( ( pData[18] & 0x3c ) >> 2 );
            iH = int(( pData[18] & 0x03   ) << 30 );
            memcpy( &iL , &pData[19] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain3 = iV / m_iK ;

            cData.m_cSstate4 = ( ( pData[23] & 0x3c ) >> 2 );
            iH = int(( pData[23] & 0x03   ) << 30 );
            memcpy( &iL , &pData[24] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain4 = iV / m_iK ;

            cData.m_cSstate5 = ( ( pData[28] & 0x3c ) >> 2 );
            iH = int(( pData[28] & 0x03   ) << 30 );
            memcpy( &iL , &pData[29] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain5 = iV / m_iK ;


            cData.m_cSstate6 = ( ( pData[33] & 0x3c ) >> 2 );
            iH = int(( pData[33] & 0x03   ) << 30 );
            memcpy( &iL , &pData[34] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain6 = iV / m_iK ;

            cData.m_cSstate7 = ( ( pData[38] & 0x3c ) >> 2 );
            iH = int(( pData[38] & 0x03   ) << 30 );
            memcpy( &iL , &pData[39] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain7 = iV / m_iK ;

            cData.m_cSstate8 = ( ( pData[43] & 0x3c ) >> 2 );
            iH = int(( pData[43] & 0x03   ) << 30 );
            memcpy( &iL , &pData[44] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain8 = iV / m_iK ;

            cData.m_cSstate9 = ( ( pData[48] & 0x3c ) >> 2 );
            iH = int(( pData[48] & 0x03   ) << 30 );
            memcpy( &iL , &pData[49] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain9 = iV / m_iK ;

            cData.m_cSstate10 = ( ( pData[53] & 0x3c ) >> 2 );
            iH = int(( pData[53] & 0x03   ) << 30 );
            memcpy( &iL , &pData[53] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            cData.m_fstrain10 = iV / m_iK ;


            cData.m_cState1 = ( ( pData[57] & 0x3c ) >> 2 );
            iH = int(( pData[57] & 0x03   ) << 30 );
            memcpy( &iL , &pData[58] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal1, &iV , 4 );
            cData.m_fGeothermal1 /= i_Temp;

            cData.m_cState2 = ( ( pData[62] & 0x3c ) >> 2 );
            iH = int(( pData[62] & 0x03   ) << 30 );
            memcpy( &iL , &pData[63] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal2 , &iV , 4 );
            cData.m_fGeothermal2 /= i_Temp;

            cData.m_cState3 = ( ( pData[67] & 0x3c ) >> 2 );
            iH = int(( pData[67] & 0x03   ) << 30 );
            memcpy( &iL , &pData[68] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal3 , &iV , 4 );
            cData.m_fGeothermal3 /= i_Temp;

            cData.m_cState4 = ( ( pData[72] & 0x3c ) >> 2 );
            iH = int(( pData[72] & 0x03   ) << 30 );
            memcpy( &iL , &pData[73] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal4 , &iV , 4 );
            cData.m_fGeothermal4 /= i_Temp;

            cData.m_cState5 = ( ( pData[77] & 0x3c ) >> 2 );
            iH = int(( pData[77] & 0x03   ) << 30 );
            memcpy( &iL , &pData[78] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal5 , &iV , 4 );
            cData.m_fGeothermal5 /= i_Temp;

            cData.m_cState6 = ( ( pData[82] & 0x3c ) >> 2 );
            iH = int(( pData[82] & 0x03   ) << 30 );
            memcpy( &iL , &pData[83] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal6 , &iV , 4 );
            cData.m_fGeothermal6 /= i_Temp;

            cData.m_cState7 = ( ( pData[87] & 0x3c ) >> 2 );
            iH = int(( pData[87] & 0x03   ) << 30 );
            memcpy( &iL , &pData[88] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal7 , &iV , 4 );
            cData.m_fGeothermal7 /= i_Temp;

            cData.m_cState8 = ( ( pData[92] & 0x3c ) >> 2 );
            iH = int(( pData[92] & 0x03   ) << 30 );
            memcpy( &iL , &pData[93] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal8 , &iV , 4 );
            cData.m_fGeothermal8 /= i_Temp;

            cData.m_cState9 = ( ( pData[97] & 0x3c ) >> 2 );
            iH = int(( pData[97] & 0x03   ) << 30 );
            memcpy( &iL , &pData[98] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal9 , &iV , 4 );
            cData.m_fGeothermal9 /= i_Temp;

            cData.m_cState10 = ( ( pData[102] & 0x3c ) >> 2 );
            iH = int(( pData[102] & 0x03   ) << 30 );
            memcpy( &iL , &pData[103] , 4 ); iL = int ( ( iL >> 2 ) );
            iV = ( iH |iL ) ;
            memcpy( & cData.m_fGeothermal10 , &iV , 4 );
            cData.m_fGeothermal10 /= i_Temp;
            */

            emit TempValueChange( cData );
            m_ReceiveBytes.remove( 0, DataLength );
        }
    }

}
void Temperature::ReceiveDataSlot()
{
     m_ReceiveBytes += m_SerialPort.readAll();
     ParamTemperature( m_ReceiveBytes );
}

void Temperature::ThreadStartInit()
{
    connect( &m_SerialPort , SIGNAL( readyRead())  , this , SLOT(ReceiveDataSlot()) );
}
