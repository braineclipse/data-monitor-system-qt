#ifndef HISTORYDATA_H
#define HISTORYDATA_H

#include <QDialog>
#include"qcustomplot.h"
#include"base_define.h"
#include<QVector>
#include<QTimer>
namespace Ui {
class HistoryData;
}

class HistoryData : public QDialog
{
    Q_OBJECT

public:
    explicit HistoryData(QWidget *parent = nullptr);
    ~HistoryData();
    /**
     * @brief InitPlot   初始化plot窗口
     * @param plot        窗口指针
     */
    void InitPlot(QCustomPlot* plot  , int offset);

    /**
     * @brief InitPlotT   初始化地温plot窗口
     * @param plot        窗口指针
     */
    void InitPlotT(QCustomPlot* plot  , int offset);
    /**
     * @brief InitPlotR  初始化北航旋转数据
     * @param plotx      X轴旋转窗口
     * @param plotY      Y轴旋转窗口
     * @param plotZ      Z轴旋转窗口
     */
    void InitPlotR( QCustomPlot *plotX ,QCustomPlot *plotY ,QCustomPlot *plotZ  );

    void UpdateDataJGP( tagData tGCdata ,  tagData tJDdata , tagIMU tIMU );
    void GetMaxMin( QVector<double> vec1 , QVector<double> vec2 ,QVector<double> vec3, QVector<double> vec4 ,QVector<double> vec5,  double &dMax, double &dMin );


    QVector<tagData>    m_tagJD;
    QVector<tagData>    m_tagGC;
    QVector<tagIMU>     m_tagBH;
    QVector<QDateTime>  m_tagT;
    QTimer              m_Timer;
signals:
    void SIGNAL_OUT_Date( QDateTime , QDateTime );
private:
    Ui::HistoryData *ui;
    int                 iStep;
      int         m_iGrahpCount ;    //图表显示数据个数
    QVector<double>      m_vecT;         //时间轴数据
    QVector<double>      m_VecTVJDS1 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS2 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS3 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS4 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS5 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS6 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS7 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS8 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS9 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS10 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT1 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT2 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT3 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT4 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT5 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT6 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT7 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT8 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT9 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT10 ; //交大纵轴数据

    QVector<double>      m_VecTVGCS1 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS2 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS3 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS4 ; //工程纵轴数据


    QVector<double>      m_VecTVGCS5 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS6 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS7 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS8 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS9 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS10 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT1 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT2 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT3 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT4 ; //工程纵轴数据


    QVector<double>      m_VecTVGCT5 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT6 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT7 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT8 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT9 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT10 ; //工程纵轴数据
    QVector<double>      m_VecTVBHX ; //北航纵轴数据
    QVector<double>      m_VecTVBHY ; //北航纵轴数据
    QVector<double>      m_VecTVBHZ ; //北航纵轴数据
    int                  m_Index;

    QPen qPenSlot[5];
public slots:
    void Slot_timer();
private slots:
    void on_PBResearch_clicked();
    void on_PBPlay_clicked();

};

#endif // HISTORYDATA_H
