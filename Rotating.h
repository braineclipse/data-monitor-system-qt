#ifndef ROTATING_H
#define ROTATING_H
#include<QSerialPort>
#include<QtSerialPort/QSerialPort>
#include <QObject>
#include"base_define.h"
class Rotating : public QObject
{
    Q_OBJECT
public:
    explicit Rotating(QObject *parent = nullptr);
    /**
     * @brief InitParam   初始化参数
     * @param strCom        串口号
     * @param iBaud       波特率
     */
    void InitParam( const QString& strCom , const int& iBaud  );
    /**
     * @brief ParamRotating 解析旋转数据
     * @param byte
     */
    void ParamRotating(QByteArray byte);
signals:


    /**
     * @brief RotateValueChange 北航数据信号
     */
    void RotateValueChange( tagIMU  );

public slots:
    /**
     * @brief ReceiveDataSlot    接收串口数据槽函数
     */
    void ReceiveDataSlot();

    /**
     * @brief ThreadStartInit    线程启动初始化
     */
    void ThreadStartInit();
private:
    QSerialPort                 m_SerialPort;
    QString                     m_strCom;
    int                         m_iBaud;
    QByteArray                  m_ReceiveBytes;
    QByteArray                  m_arrData;
};

#endif // ROTATING_H
