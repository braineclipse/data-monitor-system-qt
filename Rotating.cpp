#include "Rotating.h"


class RotationDataReader{
private:
    unsigned char *dataBlock = nullptr;
    int dataBlockSize = 0;
    // frame header magic words read from data.
    int frameHeader = 0;
    // timeStamp read from data.
    int timeStamp = 0;
    // CRC16
    unsigned short CRCCHECKSUM = 0;
    // frame tail magic words read from data.
    int frameTail = 0;
    float x;
    float y;
    float z;
public:
    void Parser(const unsigned char* pBlock, int blockSize,bool isLittleEndian);
    void GetData(float * rx,float * ry,float * rz);
    int GetTimeStamp();
    ~RotationDataReader();
};
void RotationDataReader::Parser(const unsigned char* pBlock, int blockSize,bool isLittleEndian){
    if(dataBlock == nullptr){
        dataBlock = (unsigned char *)malloc(blockSize);
        memcpy(dataBlock,pBlock,blockSize);
        dataBlockSize = blockSize;
    }
    else if(blockSize != dataBlockSize){
        free(dataBlock);
        dataBlock = (unsigned char *)malloc(blockSize);
        memcpy(dataBlock,pBlock,blockSize);
        dataBlockSize = blockSize;
    }
    frameHeader = *(int*)dataBlock;
    timeStamp = *(int*)(dataBlock + 4);
    CRCCHECKSUM = *(unsigned short*)(dataBlock + 23);
    frameTail = *(int*)(dataBlock + 25);
    for (int i=0;i<3;i++){
        unsigned char subBlock[5] = { 0, };
        //把数据做一次拷贝
        memcpy_s(&subBlock, 5, dataBlock + 8 + 5 * i, 5);
        //转换端序
        if (isLittleEndian) {
            for (int j = 0; j < 2; j++) {
                unsigned char temp = subBlock[j];
                subBlock[j] = subBlock[4 - j];
                subBlock[4 - j] = temp;
            }
        }
        //readData
        if(subBlock[0] == 0x00){
            x = *(float*)(subBlock+1);
        }
        if(subBlock[0] == 0x01){
            y = *(float*)(subBlock+1);
        }
        if(subBlock[0] == 0x10){
            z = *(float*)(subBlock+1);
        }
    }
}

void RotationDataReader::GetData(float *rx, float *ry, float *rz){
    *rx = x;
    *ry = y;
    *rz = z;
}
int RotationDataReader::GetTimeStamp(){
    return timeStamp;
}
RotationDataReader::~RotationDataReader(){
    if(dataBlock != nullptr){
        free(dataBlock);
    }
    dataBlock = nullptr;
    dataBlockSize = 0;
}


Rotating::Rotating(QObject *parent) : QObject(parent)
{
    m_ReceiveBytes.clear();
}
void Rotating::InitParam(const QString &strCom, const int &iBaud)
{
    m_strCom  = strCom;
    m_iBaud   = iBaud;

    //设置串口参数
    m_SerialPort.setPortName( m_strCom );
    m_SerialPort.setBaudRate( m_iBaud );
    m_SerialPort.setDataBits( QSerialPort::Data8 );
    m_SerialPort.setParity( QSerialPort::NoParity );
    m_SerialPort.setStopBits( QSerialPort::OneStop );
    m_SerialPort.setFlowControl( QSerialPort::NoFlowControl );

    //打开串口
    if( m_SerialPort.isOpen() )
    {
        m_SerialPort.clear();
        m_SerialPort.close();
    }
    m_SerialPort.open( QIODevice::ReadWrite );
}
void Rotating::ReceiveDataSlot()
{
     m_ReceiveBytes += m_SerialPort.readAll();
     ParamRotating( m_ReceiveBytes );
}

void Rotating::ThreadStartInit()
{
    connect( &m_SerialPort , SIGNAL( readyRead())  , this , SLOT(ReceiveDataSlot()) );
}
void Rotating::ParamRotating(QByteArray byte)
{
    int DataLength = 29 ;
    QString startHead("fffefeff");
    QByteArray arrHead =  QByteArray::fromHex( startHead.toUtf8() );
    tagIMU cData ;

    RotationDataReader rReader = RotationDataReader();
    while( m_ReceiveBytes.size() >= DataLength )
    {
        int iPos = m_ReceiveBytes.indexOf( arrHead );
        if( iPos < 0 )      // did not find head
        {
           m_ReceiveBytes.clear();
           return;
        }
        else if( iPos > 0 )     // has data before head
        {
           m_ReceiveBytes.remove( 0, iPos );   // remove data before head
        }

        while( m_ReceiveBytes.size() >= DataLength )
        {
            //uchar* pData = (uchar*)m_ReceiveBytes.data();
            //memcpy( &cData ,pData + 4 , sizeof ( tagIMU)  );
            rReader.Parser((uchar *)m_ReceiveBytes.data(),DataLength,false);
            cData.m_uiTime = rReader.GetTimeStamp();
            rReader.GetData(&cData.m_fRouteX,&cData.m_fRouteY,&cData.m_fRouteZ);
            cData.m_cStateX = 0;
            cData.m_cStateY = 1;
            cData.m_cStateZ = 2;
            emit RotateValueChange( cData );
            m_ReceiveBytes.remove( 0, DataLength );
        }
    }
}

