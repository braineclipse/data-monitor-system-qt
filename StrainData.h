#ifndef STRAINDATA_H
#define STRAINDATA_H
#include<QSerialPort>
#include<QtSerialPort/QSerialPort>
#include <QObject>
#include"base_define.h"
class StrainData : public QObject
{
    Q_OBJECT
public:
    explicit StrainData(QObject *parent = nullptr);
    /**
     * @brief InitParam   初始化参数
     * @param strCom        串口号
     * @param iBaud       波特率
     */
    void InitParam( const QString& strCom , const int& iBaud  );
    /**
     * @brief StrainData::ParamStrain  解析应变数据
     * @param byte
     */
    void ParamStrain(QByteArray byte);
signals:

    /**
     * @brief StrainValueChange  应变数据改变信号
     * @param fX                  x分量信号
     * @param fY                  y分量信号
     * @param fZ                  z分量信号
     */
    void StrainValueChange( tagData );
public slots:
    /**
     * @brief ReceiveDataSlot    接收串口数据槽函数
     */
    void ReceiveDataSlot();

    /**
     * @brief ThreadStartInit    线程启动初始化
     */
    void ThreadStartInit();
private:
    QSerialPort                 m_SerialPort;
    QString                     m_strCom;
    int                         m_iBaud;
    QByteArray                  m_ReceiveBytes;
        int                         m_iK;
};

#endif // STRAINDATA_H
