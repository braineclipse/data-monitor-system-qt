# DataMonitorSystem-QT

#### 介绍
三分量上位机显示软件，使用QT编写。

作者：哈尔滨工程大学


#### 软件架构
使用QT编写，使用开源数据库SQLite，使用开源显示库QCustomPlot。

[QCustomPlot](https://www.qcustomplot.com/) ：GPL V3

[SQLite](https://sqlite.org/index.html) : Apache V2

#### 其他

部分代码由第三方所写，我仅对缺陷点进行了修改以及更新。

