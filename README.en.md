# DataMonitorSystem-QT

#### Description
3-Component display software, programmed in C++, QT.

Author: Harbin Engineering University


#### Software Architecture
Using QT frameware.  Using SQLite as database, using opensource chart library QCustomPlot.

[QCustomPlot](https://www.qcustomplot.com/) ：GPL V3

[SQLite](https://sqlite.org/index.html) : Apache V2


#### Others

The original codes were wrote by others. I just repaired the bug and finished the project.

