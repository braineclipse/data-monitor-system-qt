#include "mainwindow.h"
#include "ui_mainwindow.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    iStep = 1;
    m_iGrahpCount = 1000;
    qRegisterMetaType<tagData>("tagData");
    qRegisterMetaType<tagIMU>("tagIMU");
    InitModel();


    m_bSqlState = false;
    bState[0] = false ; bState[1] = false ; bState[2] =false ; //0：对应交大 ， 1 ：对应工程 ， 2：对应北航
    m_winHistoryData = new HistoryData( this );
    connect( m_winHistoryData , SIGNAL(SIGNAL_OUT_Date( QDateTime , QDateTime )  ) , this , SLOT( ProHisDate( QDateTime , QDateTime )) );
    connect( ui->PBHistory , SIGNAL( clicked() ) , this , SLOT( slot_ShowHisWind() ));
    connect( &m_Timer , SIGNAL( timeout()) , this , SLOT( slot_Timer( ))  );
    m_Timer.setInterval( 1000 );//更新速率
    m_Timer.start( );

    //+ 0309 初始化画笔颜色
    for(int qPenLoop = 0;qPenLoop < 5;qPenLoop++){
        qPenSlot[qPenLoop].setWidth(2);
        qPenSlot[qPenLoop].setBrush(QBrush(Qt::SolidLine));
        qPenSlot[qPenLoop].setCapStyle(Qt::RoundCap);
        qPenSlot[qPenLoop].setJoinStyle(Qt::RoundJoin);
    }
    qPenSlot[0].setColor(QColor(0,133,188,255));
    qPenSlot[1].setColor(QColor(216,82,24,255));
    qPenSlot[2].setColor(QColor(236,176,31,255));
    qPenSlot[3].setColor(QColor(125,46,141,255));
    qPenSlot[4].setColor(QColor(118,171,47,255));
    //+ 0309 end
    InitPlot( ui->JDStrain1 , 10);
    InitPlot( ui->JDStrain2  , 15);
    InitPlot( ui->GCStrain1  , 0);
    InitPlot( ui->GCStrain2  , 5);
    InitPlotT(ui->JDGroTempature1 , 0);
    InitPlotT(ui->JDGroTempature2 , 5);
    InitPlotT(ui->GCGroTempature1 , 10 );
    InitPlotT(ui->GCGroTempature2 , 15);
    InitPlotR( ui->RoteX , ui->RoteY , ui->RoteZ );

    m_strListLog.clear();
    m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "模块初始化\r\n" );
    m_bSqlState = m_sql.createConnect();
    if( !m_bSqlState  )
    {
        m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "数据库初始化失败\r\n" );
    }
    else
    {
       m_sql.CreateTable("DataCourse");
    }


}

MainWindow::~MainWindow()
{
    m_StrainThread.exit(0);
    m_RotateThread.exit(0);
    m_TempThread.exit(0);
    m_StrainThread.wait( 10 );
    m_RotateThread.wait( 10 );
    m_TempThread.wait( 10 );
    delete ui;
}

void MainWindow::slot_ShowHisWind()
{
    m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "显示历史窗口\r\n" );
    m_winHistoryData->show();
}

void MainWindow::slot_Timer()
{
    //更新系统时间
    QString strSYSTime = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    ui->LBTime->setText( strSYSTime );

    SetLightColor( ui->LBCom1 , bState[0] );
    SetLightColor( ui->LBCom2 , bState[1] );
    SetLightColor( ui->LBCom3 , bState[2] );

    bState[0] = false ; bState[1] = false ; bState[2] =false ;
    QString strText ;
    for( int i = 0 ; i < m_strListLog.length() ; i++ )
    {
        strText += m_strListLog.at(i);
    }
    ui->PTX->setPlainText(  strText );
    // 内存泄露点.原因：添加log速度比删除快。
    //if( m_strListLog.length() > 50 )
    //{
    //    m_strListLog.removeFirst();
    //}
    // end
    // 循环删除多余条目
    while(m_strListLog.length() > 50){
        m_strListLog.removeFirst();
    }
    // end
    if( m_bSqlState )
    {
        m_sql.InsertData( "DataCourse" , m_tagJD , m_tagGC , m_tagBH );
    }
    double dMin = 0; double dMax = 0;
    //更新图表数据
    UpdateDataJG( m_tagGC , m_tagJD , m_tagBH );
   // UpdateBH( m_tagBH );
    ui->JDGroTempature1->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVJDT1 , m_VecTVJDT2  , m_VecTVJDT3 , m_VecTVJDT4 , m_VecTVJDT5 ,  dMax , dMin );
    double dRange = 0.50 * (dMax-dMin);
    double dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1 ){
        ui->JDGroTempature1->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->JDGroTempature1->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    ui->JDGroTempature1->graph(0)->setData( m_vecT , m_VecTVJDT1 );
    ui->JDGroTempature1->graph(1)->setData( m_vecT , m_VecTVJDT2 );
    ui->JDGroTempature1->graph(2)->setData( m_vecT , m_VecTVJDT3 );
    ui->JDGroTempature1->graph(3)->setData( m_vecT , m_VecTVJDT4 );
    ui->JDGroTempature1->graph(4)->setData( m_vecT , m_VecTVJDT5 );
    ui->JDGroTempature1->replot();

    ui->JDGroTempature2->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVJDT6 ,  m_VecTVJDT7 , m_VecTVJDT8 , m_VecTVJDT9 , m_VecTVJDT10 , dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->JDGroTempature2->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->JDGroTempature2->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->JDGroTempature2->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->JDGroTempature2->graph(0)->setData( m_vecT , m_VecTVJDT6 );
    ui->JDGroTempature2->graph(1)->setData( m_vecT , m_VecTVJDT7 );
    ui->JDGroTempature2->graph(2)->setData( m_vecT , m_VecTVJDT8 );
    ui->JDGroTempature2->graph(3)->setData( m_vecT , m_VecTVJDT9 );
    ui->JDGroTempature2->graph(4)->setData( m_vecT , m_VecTVJDT10 );
    ui->JDGroTempature2->replot();

    ui->JDStrain1->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVJDS1 , m_VecTVJDS2 , m_VecTVJDS3 ,m_VecTVJDS4 , m_VecTVJDS5 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->JDStrain1->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->JDStrain1->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->JDStrain1->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->JDStrain1->graph(0)->setData( m_vecT , m_VecTVJDS1 );
    ui->JDStrain1->graph(1)->setData( m_vecT , m_VecTVJDS2 );
    ui->JDStrain1->graph(2)->setData( m_vecT , m_VecTVJDS3 );
    ui->JDStrain1->graph(3)->setData( m_vecT , m_VecTVJDS4 );
    ui->JDStrain1->graph(4)->setData( m_vecT , m_VecTVJDS5 );
    ui->JDStrain1->replot();

    ui->JDStrain2->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVJDS6 , m_VecTVJDS7 , m_VecTVJDS8 ,m_VecTVJDS9 , m_VecTVJDS10 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->JDStrain2->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->JDStrain2->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->JDStrain2->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->JDStrain2->graph(0)->setData( m_vecT , m_VecTVJDS6 );
    ui->JDStrain2->graph(1)->setData( m_vecT , m_VecTVJDS7 );
    ui->JDStrain2->graph(2)->setData( m_vecT , m_VecTVJDS8 );
    ui->JDStrain2->graph(3)->setData( m_vecT , m_VecTVJDS9 );
    ui->JDStrain2->graph(4)->setData( m_vecT , m_VecTVJDS10 );
    ui->JDStrain2->replot();

    ui->GCGroTempature1->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVGCT1 , m_VecTVGCT2 , m_VecTVGCT3 ,m_VecTVGCT4 , m_VecTVGCT5 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->GCGroTempature1->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->GCGroTempature1->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->GCGroTempature1->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->GCGroTempature1->graph(0)->setData( m_vecT , m_VecTVGCT1 );
    ui->GCGroTempature1->graph(1)->setData( m_vecT , m_VecTVGCT2 );
    ui->GCGroTempature1->graph(2)->setData( m_vecT , m_VecTVGCT3 );
    ui->GCGroTempature1->graph(3)->setData( m_vecT , m_VecTVGCT4 );
    ui->GCGroTempature1->graph(4)->setData( m_vecT , m_VecTVGCT5 );
    ui->GCGroTempature1->replot();

    ui->GCGroTempature2->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVGCT6 , m_VecTVGCT7 , m_VecTVGCT8 ,m_VecTVGCT9 , m_VecTVGCT10 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->GCGroTempature2->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->GCGroTempature2->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->GCGroTempature2->yAxis->setRange( dMin -iStep , dMax+ iStep );

    ui->GCGroTempature2->graph(0)->setData( m_vecT , m_VecTVGCT6 );
    ui->GCGroTempature2->graph(1)->setData( m_vecT , m_VecTVGCT7 );
    ui->GCGroTempature2->graph(2)->setData( m_vecT , m_VecTVGCT8 );
    ui->GCGroTempature2->graph(3)->setData( m_vecT , m_VecTVGCT9 );
    ui->GCGroTempature2->graph(4)->setData( m_vecT , m_VecTVGCT10 );
    ui->GCGroTempature2->replot();

    ui->GCStrain1->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVGCS1 , m_VecTVGCS2 , m_VecTVGCS3 ,m_VecTVGCS4 , m_VecTVGCS5 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->GCStrain1->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->GCStrain1->yAxis->setRange( dAvg - dRange*1.2, dAvg + dRange*1.2 );
    }
    //ui->GCStrain1->yAxis->setRange( dMin -iStep , dMax+ iStep );

    ui->GCStrain1->graph(0)->setData( m_vecT , m_VecTVGCS1 );
    ui->GCStrain1->graph(1)->setData( m_vecT , m_VecTVGCS2 );
    ui->GCStrain1->graph(2)->setData( m_vecT , m_VecTVGCS3 );
    ui->GCStrain1->graph(3)->setData( m_vecT , m_VecTVGCS4 );
    ui->GCStrain1->graph(4)->setData( m_vecT , m_VecTVGCS5 );
    ui->GCStrain1->replot();

    ui->GCStrain2->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVGCS6 , m_VecTVGCS7 , m_VecTVGCS8 ,m_VecTVGCS9 , m_VecTVGCS10 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->GCStrain2->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->GCStrain2->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->GCStrain2->yAxis->setRange( dMin -10 , dMax+ 10 );
    ui->GCStrain2->graph(0)->setData( m_vecT , m_VecTVGCS6 );
    ui->GCStrain2->graph(1)->setData( m_vecT , m_VecTVGCS7 );
    ui->GCStrain2->graph(2)->setData( m_vecT , m_VecTVGCS8 );
    ui->GCStrain2->graph(3)->setData( m_vecT , m_VecTVGCS9 );
    ui->GCStrain2->graph(4)->setData( m_vecT , m_VecTVGCS10 );

    ui->GCStrain2->replot();

    ui->RoteX->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    auto maxX = std::max_element(std::begin(m_VecTVBHX), std::end(m_VecTVBHX));
    //最小值表示：
    auto minX = std::min_element(std::begin(m_VecTVBHX), std::end(m_VecTVBHX));
    dRange = 0.50 * (*maxX - *minX);
    dAvg = 0.5 * (*maxX + *minX);
    if(dRange<1){
        ui->RoteX->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->RoteX->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->RoteX->yAxis->setRange( *minX -iStep , *maxX+ iStep );
    ui->RoteX->graph(0)->setData( m_vecT , m_VecTVBHX );
    ui->RoteX->replot();

    ui->RoteY->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    auto maxY = std::max_element(std::begin(m_VecTVBHY), std::end(m_VecTVBHY));
    //最小值表示：
    auto minY = std::min_element(std::begin(m_VecTVBHY), std::end(m_VecTVBHY));
    dRange = 0.50 * (*maxY - *minY);
    dAvg = 0.5 * (*maxY + *minY);
    if(dRange<1){
        ui->RoteY->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->RoteY->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->RoteY->yAxis->setRange( *minY -iStep , *maxY+ iStep );
    ui->RoteY->graph(0)->setData( m_vecT , m_VecTVBHY );
    ui->RoteY->replot();

    ui->RoteZ->xAxis->setRange( QDateTime::currentSecsSinceEpoch() - 100 ,  QDateTime::currentSecsSinceEpoch() + 10 );
    auto maxZ = std::max_element(std::begin(m_VecTVBHZ), std::end(m_VecTVBHZ));
    //最小值表示：
    auto minZ = std::min_element(std::begin(m_VecTVBHZ), std::end(m_VecTVBHZ));
    dRange = 0.50 * (*maxZ - *minZ);
    dAvg = 0.5 * (*maxZ + *minZ);
    if(dRange<1 ){
        ui->RoteZ->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->RoteZ->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->RoteZ->yAxis->setRange( *minZ -iStep , *maxZ+ iStep );
    ui->RoteZ->graph(0)->setData( m_vecT , m_VecTVBHZ );
    ui->RoteZ->replot();

}

void MainWindow::slot_ReceiveJD(tagData data)
{
    m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "接收交大数据\r\n" );
    m_tagJD = data;
    bState[0] = true;
}

void MainWindow::slot_ReceiveRotateSlot(tagIMU IMUData)
{
    m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "接收工程数据\r\n" );
    bState[2] = true;
    m_tagBH = IMUData;
}

void MainWindow::slot_ReceiveGC(tagData data)
{
    m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "接收北航数据\r\n" );
    bState[1] = true;
    m_tagGC = data;
}

void MainWindow::UpdateDataJG(tagData tGCdata ,  tagData tJDdata  , tagIMU tIMU)
{
    tJDdata.m_uiTime = QDateTime::currentSecsSinceEpoch();
    if( m_vecT.size() >= m_iGrahpCount )
    {
        m_vecT.removeFirst();
        m_vecT.append( tJDdata.m_uiTime );
        m_VecTVJDS1.append( tJDdata.m_fstrain1 );
        m_VecTVJDS2.append( tJDdata.m_fstrain2 );
        m_VecTVJDS3.append( tJDdata.m_fstrain3 );
        m_VecTVJDS4.append( tJDdata.m_fstrain4 );
        m_VecTVJDS5.append( tJDdata.m_fstrain5 );
        m_VecTVJDS6.append( tJDdata.m_fstrain6 );
        m_VecTVJDS7.append( tJDdata.m_fstrain7 );
        m_VecTVJDS8.append( tJDdata.m_fstrain8 );
        m_VecTVJDS9.append( tJDdata.m_fstrain9 );
        m_VecTVJDS10.append( tJDdata.m_fstrain10 );
        m_VecTVJDS1.removeFirst();
        m_VecTVJDS2.removeFirst();
        m_VecTVJDS3.removeFirst();
        m_VecTVJDS4.removeFirst();
        m_VecTVJDS5.removeFirst();
        m_VecTVJDS6.removeFirst();
        m_VecTVJDS7.removeFirst();
        m_VecTVJDS8.removeFirst();

        m_VecTVJDS9.removeFirst();
        m_VecTVJDS10.removeFirst();

        m_VecTVJDT1.append( tJDdata.m_fGeothermal1 );
        m_VecTVJDT1.removeFirst();
        m_VecTVJDT2.append( tJDdata.m_fGeothermal2 );
        m_VecTVJDT2.removeFirst();
        m_VecTVJDT3.append( tJDdata.m_fGeothermal3 );
        m_VecTVJDT3.removeFirst();
        m_VecTVJDT4.append( tJDdata.m_fGeothermal4 );
        m_VecTVJDT4.removeFirst();
        m_VecTVJDT5.append( tJDdata.m_fGeothermal5 );
        m_VecTVJDT5.removeFirst();
        m_VecTVJDT6.append( tJDdata.m_fGeothermal6 );
        m_VecTVJDT6.removeFirst();
        m_VecTVJDT7.append( tJDdata.m_fGeothermal7 );
        m_VecTVJDT7.removeFirst();
        m_VecTVJDT8.append( tJDdata.m_fGeothermal8 );
        m_VecTVJDT8.removeFirst();
        m_VecTVJDT9.append( tJDdata.m_fGeothermal9 );
        m_VecTVJDT9.removeFirst();
        m_VecTVJDT10.append( tJDdata.m_fGeothermal10 );
        m_VecTVJDT10.removeFirst();
        m_VecTVGCS1.append( tGCdata.m_fstrain1 );
        m_VecTVGCS2.append( tGCdata.m_fstrain2 );
        m_VecTVGCS3.append( tGCdata.m_fstrain3 );
        m_VecTVGCS4.append( tGCdata.m_fstrain4 );

        m_VecTVGCS5.append( tGCdata.m_fstrain5 );
        m_VecTVGCS6.append( tGCdata.m_fstrain6 );
        m_VecTVGCS7.append( tGCdata.m_fstrain7 );
        m_VecTVGCS8.append( tGCdata.m_fstrain8 );
        m_VecTVGCS9.append( tGCdata.m_fstrain9 );
        m_VecTVGCS10.append( tGCdata.m_fstrain10 );
        m_VecTVGCS1.removeFirst();
        m_VecTVGCS2.removeFirst();
        m_VecTVGCS3.removeFirst();
        m_VecTVGCS4.removeFirst();
        m_VecTVGCS5.removeFirst();
        m_VecTVGCS6.removeFirst();
        m_VecTVGCS7.removeFirst();
        m_VecTVGCS8.removeFirst();

        m_VecTVGCS9.removeFirst();
        m_VecTVGCS10.removeFirst();

        m_VecTVGCT1.append( tGCdata.m_fGeothermal1 );
        m_VecTVGCT1.removeFirst();
        m_VecTVGCT2.append( tGCdata.m_fGeothermal2 );
        m_VecTVGCT2.removeFirst();
        m_VecTVGCT3.append( tGCdata.m_fGeothermal3 );
        m_VecTVGCT3.removeFirst();
        m_VecTVGCT4.append( tGCdata.m_fGeothermal4 );
        m_VecTVGCT4.removeFirst();
        m_VecTVGCT5.append( tGCdata.m_fGeothermal5 );
        m_VecTVGCT5.removeFirst();
        m_VecTVGCT6.append( tGCdata.m_fGeothermal6 );
        m_VecTVGCT6.removeFirst();
        m_VecTVGCT7.append( tGCdata.m_fGeothermal7 );
        m_VecTVGCT7.removeFirst();
        m_VecTVGCT8.append( tGCdata.m_fGeothermal8 );
        m_VecTVGCT8.removeFirst();
        m_VecTVGCT9.append( tGCdata.m_fGeothermal9 );
        m_VecTVGCT9.removeFirst();
        m_VecTVGCT10.append( tGCdata.m_fGeothermal10 );
        m_VecTVGCT10.removeFirst();

        m_VecTVBHX.append( tIMU.m_fRouteX );
        m_VecTVBHY.append( tIMU.m_fRouteY );
        m_VecTVBHZ.append( tIMU.m_fRouteZ );

        m_VecTVBHX.removeFirst();
        m_VecTVBHY.removeFirst();
        m_VecTVBHZ.removeFirst();
    }
    else
    {
        m_vecT.append( tJDdata.m_uiTime );
        m_VecTVJDS1.append( tJDdata.m_fstrain1 );
        m_VecTVJDS2.append( tJDdata.m_fstrain2 );
        m_VecTVJDS3.append( tJDdata.m_fstrain3 );
        m_VecTVJDS4.append( tJDdata.m_fstrain4 );

        m_VecTVJDS5.append( tJDdata.m_fstrain5 );
        m_VecTVJDS6.append( tJDdata.m_fstrain6 );
        m_VecTVJDS7.append( tJDdata.m_fstrain7 );
        m_VecTVJDS8.append( tJDdata.m_fstrain8 );
        m_VecTVJDS9.append( tJDdata.m_fstrain9 );
        m_VecTVJDS10.append( tJDdata.m_fstrain10 );

        m_VecTVJDT1.append( tJDdata.m_fGeothermal1 );
        m_VecTVJDT2.append( tJDdata.m_fGeothermal2 );
        m_VecTVJDT3.append( tJDdata.m_fGeothermal3 );
        m_VecTVJDT4.append( tJDdata.m_fGeothermal4 );
        m_VecTVJDT5.append( tJDdata.m_fGeothermal5 );
        m_VecTVJDT6.append( tJDdata.m_fGeothermal6 );
        m_VecTVJDT7.append( tJDdata.m_fGeothermal7 );
        m_VecTVJDT8.append( tJDdata.m_fGeothermal8 );
        m_VecTVJDT9.append( tJDdata.m_fGeothermal9 );
        m_VecTVJDT10.append( tJDdata.m_fGeothermal10 );
        m_VecTVGCS1.append( tGCdata.m_fstrain1 );
        m_VecTVGCS2.append( tGCdata.m_fstrain2 );
        m_VecTVGCS3.append( tGCdata.m_fstrain3 );
        m_VecTVGCS4.append( tGCdata.m_fstrain4 );

        m_VecTVGCS5.append( tGCdata.m_fstrain5 );
        m_VecTVGCS6.append( tGCdata.m_fstrain6 );
        m_VecTVGCS7.append( tGCdata.m_fstrain7 );
        m_VecTVGCS8.append( tGCdata.m_fstrain8 );
        m_VecTVGCS9.append( tGCdata.m_fstrain9 );
        m_VecTVGCS10.append( tGCdata.m_fstrain10 );

        m_VecTVGCT1.append( tGCdata.m_fGeothermal1 );
        m_VecTVGCT2.append( tGCdata.m_fGeothermal2 );
        m_VecTVGCT3.append( tGCdata.m_fGeothermal3 );
        m_VecTVGCT4.append( tGCdata.m_fGeothermal4 );
        m_VecTVGCT5.append( tGCdata.m_fGeothermal5 );
        m_VecTVGCT6.append( tGCdata.m_fGeothermal6 );
        m_VecTVGCT7.append( tGCdata.m_fGeothermal7 );
        m_VecTVGCT8.append( tGCdata.m_fGeothermal8 );
        m_VecTVGCT9.append( tGCdata.m_fGeothermal9 );
        m_VecTVGCT10.append( tGCdata.m_fGeothermal10 );
        m_VecTVBHX.append( tIMU.m_fRouteX );
        m_VecTVBHY.append( tIMU.m_fRouteY );
        m_VecTVBHZ.append( tIMU.m_fRouteZ );

    }
}

void MainWindow::UpdateBH(tagIMU tIMU)
{
    if( m_vecT.size() >= m_iGrahpCount )
    {

        m_VecTVBHX.append( tIMU.m_fRouteX );
        m_VecTVBHY.append( tIMU.m_fRouteY );
        m_VecTVBHZ.append( tIMU.m_fRouteZ );

        m_VecTVBHX.removeFirst();
        m_VecTVBHY.removeFirst();
        m_VecTVBHZ.removeFirst();

    }
    else
    {
        m_VecTVBHX.append( tIMU.m_fRouteX );
        m_VecTVBHY.append( tIMU.m_fRouteY );
        m_VecTVBHZ.append( tIMU.m_fRouteZ );

    }
}

void MainWindow::GetMaxMin(QVector<double> vec1, QVector<double> vec2, QVector<double> vec3, QVector<double> vec4, QVector<double> vec5, double &dMax, double &dMin)
{
    QVector<double> dVecMin;
    QVector<double> dVecMax;
    auto max1 = std::max_element(std::begin(vec1), std::end(vec1));
    //最小值表示：
    auto min1 = std::min_element(std::begin(vec1), std::end(vec1));
    dVecMin.append( *min1 );
    dVecMax.append( *max1 );
    auto max2 = std::max_element(std::begin(vec2), std::end(vec2));
    //最小值表示：
    auto min2 = std::min_element(std::begin(vec2), std::end(vec2));

    dVecMin.append( *min2 );
    dVecMax.append( *max2 );

    auto max3 = std::max_element(std::begin(vec3), std::end(vec3));
    //最小值表示：
    auto min3 = std::min_element(std::begin(vec3), std::end(vec3));

    dVecMin.append( *min3 );
    dVecMax.append( *max3 );

    auto max4 = std::max_element(std::begin(vec4), std::end(vec4));
    //最小值表示：
    auto min4 = std::min_element(std::begin(vec4), std::end(vec4));

    dVecMin.append( *min4 );
    dVecMax.append( *max4 );

    auto max5 = std::max_element(std::begin(vec5), std::end(vec5));
    //最小值表示：
    auto min5 = std::min_element(std::begin(vec5), std::end(vec5));

    dVecMin.append( *min5 );
    dVecMax.append( *max5 );

    auto tMax = std::max_element(std::begin(dVecMax), std::end(dVecMax));
    //最小值表示：
    auto tMin = std::min_element(std::begin(dVecMin), std::end(dVecMin));

    dMax = *tMax;
    dMin = *tMin;

}

void MainWindow::ProHisDate(QDateTime tDateS, QDateTime tDateT)
{
    m_sql.ReadSearchData( "DataCourse" , tDateS , tDateT );
    SetData();
}

void MainWindow::InitModel()
{
    QString configFilePath = QApplication::applicationDirPath() + "/config.ini";
    m_SettingConf.InitConfig(configFilePath);
    QString strCom ; int iBaud;
    strCom = m_SettingConf.ReadSetting( "ParamCom" , "COM1"  );
    iBaud = m_SettingConf.ReadSetting( "ParamCom" , "Baud1"  ).toInt();
    m_StrainData.InitParam(strCom , iBaud  );

    strCom = m_SettingConf.ReadSetting( "ParamCom" , "COM2"  );
    iBaud = m_SettingConf.ReadSetting( "ParamCom" , "Baud2"  ).toInt();
    m_Rotating.InitParam(strCom , iBaud  );

    strCom = m_SettingConf.ReadSetting( "ParamCom" , "COM3"  );
    iBaud = m_SettingConf.ReadSetting( "ParamCom" , "Baud3"  ).toInt();
    m_Temperature.InitParam(strCom , iBaud  );

    m_StrainData.moveToThread( &m_StrainThread );
    m_Rotating.moveToThread( &m_RotateThread );
    m_Temperature.moveToThread( &m_TempThread );


    connect(  &m_StrainThread , SIGNAL( started() ) ,&m_StrainData , SLOT( ThreadStartInit() )  );

    connect(  &m_RotateThread , SIGNAL( started() ) ,&m_Rotating , SLOT( ThreadStartInit() )  );

    connect(  &m_TempThread , SIGNAL( started() ) ,&m_Temperature , SLOT( ThreadStartInit() )  );

    connect( &m_StrainData , SIGNAL(StrainValueChange( tagData  )  ), this , SLOT( slot_ReceiveJD(tagData)) );
    connect( &m_Rotating , SIGNAL( RotateValueChange(  tagIMU) ) , this, SLOT( slot_ReceiveRotateSlot( tagIMU ) ) );
    connect( &m_Temperature , SIGNAL( TempValueChange( tagData ) ), this, SLOT( slot_ReceiveGC(tagData) ) );
    m_StrainThread.start();
    m_RotateThread.start();
    m_TempThread.start();



     m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "读取配置文件\r\n" );

}

void MainWindow::InitPlot( QCustomPlot *plot  ,int offset)
{
     m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "初始化应变信息\r\n" );
    for( int i = 0 ; i < 5 ; i++ )
    {
        plot->legend->setVisible(true);
      //  plot->legend->setFont(QFont("Helvetica",9));
        // set locale to english, so we get english decimal separator:
       // plot->setLocale(QLocale(QLocale::English, QLocale::UnitedKingdom));
        plot->addGraph() ;
    }
    // old init style
    // plot->graph( 0 )->setPen( QPen( Qt::red ) );
    //plot->graph( 1 )->setPen( QPen( Qt::blue ) );
    //plot->graph( 2 )->setPen( QPen( Qt::green ) );
    //plot->graph( 3 )->setPen( QPen( Qt::black ) );
    //plot->graph( 4 )->setPen( QPen( Qt::cyan ) );
    // + 0309 begin..
    for(int i =0;i<5;i++){
        plot->graph(i)->setPen(qPenSlot[i]);
    }
    // + 0309 end
    for( int i = 0 ; i < 5 ; i++ )
    {
        QString strName = QString( "应变%1" ).arg( i +1 + offset );
        plot->graph(i )->setName( strName );
    }

    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);//日期做X轴
    dateTicker->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plot->xAxis->setTicker(dateTicker);//设置X轴为时间轴
    plot->xAxis->setUpperEnding(QCPLineEnding::esBar);
    plot->xAxis->setRange( 0 , 12*2400 );
    plot->xAxis->setLabel("时间");

    plot->yAxis->setUpperEnding(QCPLineEnding::esBar);
    plot->yAxis->setLabel("应变(nε)");

    QFont axisFont;
    axisFont.setFamily("Times New Roman");
    axisFont.setPointSize(12);
    plot->xAxis->setTickLabelFont(axisFont);
    plot->yAxis->setTickLabelFont(axisFont);
    QFont labelFont;
    axisFont.setFamily("SimHei");
    axisFont.setPointSize(12);
    plot->xAxis->setLabelFont(axisFont);
    plot->yAxis->setLabelFont(axisFont);

}

void MainWindow::InitPlotT(QCustomPlot *plot , int offset)
{
    m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "初始化温度信息\r\n" );
    for( int i = 0 ; i < 5 ; i++ )
    {
        plot->legend->setVisible(true);
        plot->addGraph() ;
    }
    // old init
    //plot->graph( 0 )->setPen( QPen( Qt::red ) );
    //plot->graph( 1 )->setPen( QPen( Qt::blue ) );
    //plot->graph( 2 )->setPen( QPen( Qt::green ) );
    //plot->graph( 3 )->setPen( QPen( Qt::black ) );
    //plot->graph( 4 )->setPen( QPen( Qt::cyan ) );
    // + 0309 begin..
    for(int i =0;i<5;i++){
        plot->graph(i)->setPen(qPenSlot[i]);
    }
    // + 0309 end
    for( int i = 0 ; i < 5 ; i++ )
    {
        QString strName = QString( "温度%1" ).arg( i+1 + offset );
        plot->graph(i )->setName( strName );
    }

    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);//日期做X轴
    dateTicker->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plot->xAxis->setTicker(dateTicker);//设置X轴为时间轴
    plot->xAxis->setUpperEnding(QCPLineEnding::esBar);
    plot->yAxis->setUpperEnding(QCPLineEnding::esBar);
    plot->xAxis->setLabel("时间");
    plot->yAxis->setLabel("温度(m℃)");

    QFont axisFont;
    axisFont.setFamily("Times New Roman");
    axisFont.setPointSize(12);
    plot->xAxis->setTickLabelFont(axisFont);
    plot->yAxis->setTickLabelFont(axisFont);
    QFont labelFont;
    axisFont.setFamily("SimHei");
    axisFont.setPointSize(12);
    plot->xAxis->setLabelFont(axisFont);
    plot->yAxis->setLabelFont(axisFont);

}

void MainWindow::InitPlotR(QCustomPlot *plotX ,QCustomPlot *plotY ,QCustomPlot *plotZ )
{

    m_strListLog.append( QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ") + "初始化转动信息\r\n" );
    plotX->addGraph();
    plotX->legend->setVisible(true);
    plotX->graph(0)->setPen( qPenSlot[0] );
    plotX->graph(0 )->setName( "Rx" );
    plotX->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTickerX(new QCPAxisTickerDateTime);//日期做X轴
    dateTickerX->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plotX->xAxis->setTicker(dateTickerX);//设置X轴为时间轴
    plotX->xAxis->setUpperEnding(QCPLineEnding::esBar);
    plotX->yAxis->setUpperEnding(QCPLineEnding::esBar);
    plotX->xAxis->setLabel("时间");
    plotX->yAxis->setLabel("x轴旋转(°)");


    plotY->addGraph();
    plotY->legend->setVisible(true);
    plotY->graph(0)->setPen( qPenSlot[0] );
    plotY->graph(0 )->setName( "Ry" );
    plotY->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTickerY(new QCPAxisTickerDateTime);//日期做X轴
    dateTickerY->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plotY->xAxis->setTicker(dateTickerY);//设置X轴为时间轴
    plotY->xAxis->setUpperEnding(QCPLineEnding::esBar);
    plotY->yAxis->setUpperEnding(QCPLineEnding::esBar);
    plotY->xAxis->setLabel("时间");
    plotY->yAxis->setLabel("y轴旋转(°)");


    plotZ->addGraph();
    plotZ->legend->setVisible(true);
    plotZ->graph(0)->setPen( qPenSlot[0] );
    plotZ->graph(0 )->setName( "Rz" );
    plotZ->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTickerZ(new QCPAxisTickerDateTime);//日期做X轴
    dateTickerZ->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plotZ->xAxis->setTicker(dateTickerZ);//设置X轴为时间轴
    plotZ->xAxis->setUpperEnding(QCPLineEnding::esBar);
    plotZ->yAxis->setUpperEnding(QCPLineEnding::esBar);
    plotZ->xAxis->setLabel("时间");
    plotZ->yAxis->setLabel("Z轴旋转(°)");

    QFont axisFont;
    axisFont.setFamily("Times New Roman");
    axisFont.setPointSize(12);
    plotX->xAxis->setTickLabelFont(axisFont);
    plotX->yAxis->setTickLabelFont(axisFont);
    plotY->xAxis->setTickLabelFont(axisFont);
    plotY->yAxis->setTickLabelFont(axisFont);
    plotZ->xAxis->setTickLabelFont(axisFont);
    plotZ->yAxis->setTickLabelFont(axisFont);
    QFont labelFont;
    axisFont.setFamily("SimHei");
    axisFont.setPointSize(12);
    plotX->xAxis->setLabelFont(axisFont);
    plotX->yAxis->setLabelFont(axisFont);
    plotY->xAxis->setLabelFont(axisFont);
    plotY->yAxis->setLabelFont(axisFont);
    plotZ->xAxis->setLabelFont(axisFont);
    plotZ->yAxis->setLabelFont(axisFont);

}

void MainWindow::SetLightColor(QLabel *lb , bool bState)
{
    if( bState )
    {
     //   lb->setStyleSheet( "border-radius:17px;background-color: rgb(50, 100, 50);" );  // 数据灯变成暗绿色
        lb->setText( "正常" );
    }
    else
    {
        //lb->setStyleSheet( "border-radius:17px;background-color: rgb(170, 170, 170);" );
     lb->setText( "异常" );
    }
}

void MainWindow::SetData()
{
    m_winHistoryData->m_tagJD = m_sql.m_VecJD;
    m_winHistoryData->m_tagGC = m_sql.m_VecGC;
    m_winHistoryData->m_tagBH = m_sql.m_VecBH;
    m_winHistoryData->m_tagT = m_sql.m_DateT;
}

