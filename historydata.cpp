#include "historydata.h"
#include "ui_historydata.h"

HistoryData::HistoryData(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HistoryData)
{
    ui->setupUi(this);
   // setWindowFlags(Qt::Widget);

    //+ 0310 初始化画笔颜色
    for(int qPenLoop = 0;qPenLoop < 5;qPenLoop++){
        qPenSlot[qPenLoop].setWidth(2);
        qPenSlot[qPenLoop].setBrush(QBrush(Qt::SolidLine));
        qPenSlot[qPenLoop].setCapStyle(Qt::RoundCap);
        qPenSlot[qPenLoop].setJoinStyle(Qt::RoundJoin);
    }
    qPenSlot[0].setColor(QColor(0,133,188,255));
    qPenSlot[1].setColor(QColor(216,82,24,255));
    qPenSlot[2].setColor(QColor(236,176,31,255));
    qPenSlot[3].setColor(QColor(125,46,141,255));
    qPenSlot[4].setColor(QColor(118,171,47,255));
    //+ 0310 end

    InitPlot( ui->JDHStrain1 , 10);
    InitPlot( ui->JDHStrain2  , 15);
    InitPlot( ui->HGCStrain1  , 0);
    InitPlot( ui->HGCStrain2  , 5);
    InitPlotT(ui->JDHGroTempature1 , 0);
    InitPlotT(ui->JDHGroTempature2 , 5);
    InitPlotT(ui->HGCGroTempature1 , 10 );
    InitPlotT(ui->HGCGroTempature2 , 15);
    InitPlotR( ui->HRoteX , ui->HRoteY , ui->HRoteZ );

    ui->PBResearch->setEnabled( true );
    ui->PBPlay->setEnabled( false );
    m_Index = -1;
    iStep = 150;

    m_iGrahpCount = 1000;
    connect( &m_Timer , SIGNAL( timeout() ) , this , SLOT( Slot_timer() ) );
    m_Timer.setInterval( 1000 );
}

HistoryData::~HistoryData()
{
    delete ui;
}

void HistoryData::InitPlot(QCustomPlot *plot, int offset)
{
    for( int i = 0 ; i < 5 ; i++ )
    {
        plot->legend->setVisible(true);
        plot->addGraph() ;
    }
    for(int i =0;i<5;i++){
        plot->graph(i)->setPen(qPenSlot[i]);
    }

    for( int i = 0 ; i < 5 ; i++ )
    {

        QString strName = QString( "应变%1" ).arg( i +1 + offset );
        plot->graph(i )->setName( strName );
    }

    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);//日期做X轴
    dateTicker->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plot->xAxis->setTicker(dateTicker);//设置X轴为时间轴

    plot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    plot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

    plot->xAxis->setRange( 0 , 12*2400 );
    plot->xAxis->setLabel("时间");
    plot->yAxis->setLabel("应变(nε)");

    QFont axisFont;
    axisFont.setFamily("Times New Roman");
    axisFont.setPointSize(12);
    plot->xAxis->setTickLabelFont(axisFont);
    plot->yAxis->setTickLabelFont(axisFont);
    QFont labelFont;
    axisFont.setFamily("SimHei");
    axisFont.setPointSize(12);
    plot->xAxis->setLabelFont(axisFont);
    plot->yAxis->setLabelFont(axisFont);
}

void HistoryData::InitPlotT(QCustomPlot *plot, int offset)
{
    for( int i = 0 ; i < 5 ; i++ )
    {
        plot->legend->setVisible(true);
        plot->addGraph() ;
    }
    for(int i =0;i<5;i++){
        plot->graph(i)->setPen(qPenSlot[i]);
    }
    for( int i = 0 ; i < 5 ; i++ )
    {
        QString strName = QString( "温度%1" ).arg( i+1 + offset );
        plot->graph(i )->setName( strName );
    }

    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);//日期做X轴
    dateTicker->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plot->xAxis->setTicker(dateTicker);//设置X轴为时间轴

    plot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    plot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

    plot->xAxis->setLabel("时间");
    plot->yAxis->setLabel("温度(m℃)");
    QFont axisFont;
    axisFont.setFamily("Times New Roman");
    axisFont.setPointSize(12);
    plot->xAxis->setTickLabelFont(axisFont);
    plot->yAxis->setTickLabelFont(axisFont);
    QFont labelFont;
    axisFont.setFamily("SimHei");
    axisFont.setPointSize(12);
    plot->xAxis->setLabelFont(axisFont);
    plot->yAxis->setLabelFont(axisFont);

}

void HistoryData::InitPlotR(QCustomPlot *plotX, QCustomPlot *plotY, QCustomPlot *plotZ)
{
    plotX->addGraph();
    plotX->legend->setVisible(true);
    plotX->graph(0)->setPen(  qPenSlot[0] );
    plotX->graph(0 )->setName( "Rx" );
    plotX->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTickerX(new QCPAxisTickerDateTime);//日期做X轴
    dateTickerX->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plotX->xAxis->setTicker(dateTickerX);//设置X轴为时间轴
    plotX->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    plotX->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    plotX->xAxis->setLabel("时间");
    plotX->yAxis->setLabel("x轴旋转(°)");


    plotY->addGraph();
    plotY->legend->setVisible(true);
    plotY->graph(0)->setPen(  qPenSlot[0] );
    plotY->graph(0 )->setName( "Ry" );
    plotY->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTickerY(new QCPAxisTickerDateTime);//日期做X轴
    dateTickerY->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plotY->xAxis->setTicker(dateTickerY);//设置X轴为时间轴
    plotY->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    plotY->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    plotY->xAxis->setLabel("时间");
    plotY->yAxis->setLabel("y轴旋转(°)");


    plotZ->addGraph();
    plotZ->legend->setVisible(true);
    plotZ->graph(0)->setPen( qPenSlot[0] );
    plotZ->graph(0 )->setName( "Rz" );
    plotZ->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);//添加鼠标拖放支持、滚轮缩放支持
    QSharedPointer<QCPAxisTickerDateTime> dateTickerZ(new QCPAxisTickerDateTime);//日期做X轴
    dateTickerZ->setDateTimeFormat("hh:mm:ss");//日期格式(yy-MM-dd hh:mm:ss)
    plotZ->xAxis->setTicker(dateTickerZ);//设置X轴为时间轴
    plotZ->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    plotZ->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    plotZ->xAxis->setLabel("时间");
    plotZ->yAxis->setLabel("Z轴旋转(°)");

    QFont axisFont;
    axisFont.setFamily("Times New Roman");
    axisFont.setPointSize(12);
    plotX->xAxis->setTickLabelFont(axisFont);
    plotX->yAxis->setTickLabelFont(axisFont);
    plotY->xAxis->setTickLabelFont(axisFont);
    plotY->yAxis->setTickLabelFont(axisFont);
    plotZ->xAxis->setTickLabelFont(axisFont);
    plotZ->yAxis->setTickLabelFont(axisFont);
    QFont labelFont;
    axisFont.setFamily("SimHei");
    axisFont.setPointSize(12);
    plotX->xAxis->setLabelFont(axisFont);
    plotX->yAxis->setLabelFont(axisFont);
    plotY->xAxis->setLabelFont(axisFont);
    plotY->yAxis->setLabelFont(axisFont);
    plotZ->xAxis->setLabelFont(axisFont);
    plotZ->yAxis->setLabelFont(axisFont);
}

void HistoryData::UpdateDataJGP(tagData tGCdata, tagData tJDdata, tagIMU tIMU)
{

    tJDdata.m_uiTime = m_tagT.at(m_Index).toSecsSinceEpoch();
    if( m_vecT.size() >= m_iGrahpCount )
    {
        m_vecT.removeFirst();
        m_vecT.append( tJDdata.m_uiTime );
        m_VecTVJDS1.append( tJDdata.m_fstrain1 );
        m_VecTVJDS2.append( tJDdata.m_fstrain2 );
        m_VecTVJDS3.append( tJDdata.m_fstrain3 );
        m_VecTVJDS4.append( tJDdata.m_fstrain4 );

        m_VecTVJDS5.append( tJDdata.m_fstrain5 );
        m_VecTVJDS6.append( tJDdata.m_fstrain6 );
        m_VecTVJDS7.append( tJDdata.m_fstrain7 );
        m_VecTVJDS8.append( tJDdata.m_fstrain8 );
        m_VecTVJDS9.append( tJDdata.m_fstrain9 );
        m_VecTVJDS10.append( tJDdata.m_fstrain10 );
        m_VecTVJDS1.removeFirst();
        m_VecTVJDS2.removeFirst();
        m_VecTVJDS3.removeFirst();
        m_VecTVJDS4.removeFirst();
        m_VecTVJDS5.removeFirst();
        m_VecTVJDS6.removeFirst();
        m_VecTVJDS7.removeFirst();
        m_VecTVJDS8.removeFirst();

        m_VecTVJDS9.removeFirst();
        m_VecTVJDS10.removeFirst();

        m_VecTVJDT1.append( tJDdata.m_fGeothermal1 );
        m_VecTVJDT1.removeFirst();
        m_VecTVJDT2.append( tJDdata.m_fGeothermal2 );
        m_VecTVJDT2.removeFirst();
        m_VecTVJDT3.append( tJDdata.m_fGeothermal3 );
        m_VecTVJDT3.removeFirst();
        m_VecTVJDT4.append( tJDdata.m_fGeothermal4 );
        m_VecTVJDT4.removeFirst();
        m_VecTVJDT5.append( tJDdata.m_fGeothermal5 );
        m_VecTVJDT5.removeFirst();
        m_VecTVJDT6.append( tJDdata.m_fGeothermal6 );
        m_VecTVJDT6.removeFirst();
        m_VecTVJDT7.append( tJDdata.m_fGeothermal7 );
        m_VecTVJDT7.removeFirst();
        m_VecTVJDT8.append( tJDdata.m_fGeothermal8 );
        m_VecTVJDT8.removeFirst();
        m_VecTVJDT9.append( tJDdata.m_fGeothermal9 );
        m_VecTVJDT9.removeFirst();
        m_VecTVJDT10.append( tJDdata.m_fGeothermal10 );
        m_VecTVJDT10.removeFirst();
        m_VecTVGCS1.append( tGCdata.m_fstrain1 );
        m_VecTVGCS2.append( tGCdata.m_fstrain2 );
        m_VecTVGCS3.append( tGCdata.m_fstrain3 );
        m_VecTVGCS4.append( tGCdata.m_fstrain4 );

        m_VecTVGCS5.append( tGCdata.m_fstrain5 );
        m_VecTVGCS6.append( tGCdata.m_fstrain6 );
        m_VecTVGCS7.append( tGCdata.m_fstrain7 );
        m_VecTVGCS8.append( tGCdata.m_fstrain8 );
        m_VecTVGCS9.append( tGCdata.m_fstrain9 );
        m_VecTVGCS10.append( tGCdata.m_fstrain10 );
        m_VecTVGCS1.removeFirst();
        m_VecTVGCS2.removeFirst();
        m_VecTVGCS3.removeFirst();
        m_VecTVGCS4.removeFirst();
        m_VecTVGCS5.removeFirst();
        m_VecTVGCS6.removeFirst();
        m_VecTVGCS7.removeFirst();
        m_VecTVGCS8.removeFirst();

        m_VecTVGCS9.removeFirst();
        m_VecTVGCS10.removeFirst();

        m_VecTVGCT1.append( tGCdata.m_fGeothermal1 );
        m_VecTVGCT1.removeFirst();
        m_VecTVGCT2.append( tGCdata.m_fGeothermal2 );
        m_VecTVGCT2.removeFirst();
        m_VecTVGCT3.append( tGCdata.m_fGeothermal3 );
        m_VecTVGCT3.removeFirst();
        m_VecTVGCT4.append( tGCdata.m_fGeothermal4 );
        m_VecTVGCT4.removeFirst();
        m_VecTVGCT5.append( tGCdata.m_fGeothermal5 );
        m_VecTVGCT5.removeFirst();
        m_VecTVGCT6.append( tGCdata.m_fGeothermal6 );
        m_VecTVGCT6.removeFirst();
        m_VecTVGCT7.append( tGCdata.m_fGeothermal7 );
        m_VecTVGCT7.removeFirst();
        m_VecTVGCT8.append( tGCdata.m_fGeothermal8 );
        m_VecTVGCT8.removeFirst();
        m_VecTVGCT9.append( tGCdata.m_fGeothermal9 );
        m_VecTVGCT9.removeFirst();
        m_VecTVGCT10.append( tGCdata.m_fGeothermal10 );
        m_VecTVGCT10.removeFirst();

        m_VecTVBHX.append( tIMU.m_fRouteX );
        m_VecTVBHY.append( tIMU.m_fRouteY );
        m_VecTVBHZ.append( tIMU.m_fRouteZ );

        m_VecTVBHX.removeFirst();
        m_VecTVBHY.removeFirst();
        m_VecTVBHZ.removeFirst();
    }
    else
    {
        m_vecT.append( tJDdata.m_uiTime );
        m_VecTVJDS1.append( tJDdata.m_fstrain1 );
        m_VecTVJDS2.append( tJDdata.m_fstrain2 );
        m_VecTVJDS3.append( tJDdata.m_fstrain3 );
        m_VecTVJDS4.append( tJDdata.m_fstrain4 );

        m_VecTVJDS5.append( tJDdata.m_fstrain5 );
        m_VecTVJDS6.append( tJDdata.m_fstrain6 );
        m_VecTVJDS7.append( tJDdata.m_fstrain7 );
        m_VecTVJDS8.append( tJDdata.m_fstrain8 );
        m_VecTVJDS9.append( tJDdata.m_fstrain9 );
        m_VecTVJDS10.append( tJDdata.m_fstrain10 );

        m_VecTVJDT1.append( tJDdata.m_fGeothermal1 );
        m_VecTVJDT2.append( tJDdata.m_fGeothermal2 );
        m_VecTVJDT3.append( tJDdata.m_fGeothermal3 );
        m_VecTVJDT4.append( tJDdata.m_fGeothermal4 );
        m_VecTVJDT5.append( tJDdata.m_fGeothermal5 );
        m_VecTVJDT6.append( tJDdata.m_fGeothermal6 );
        m_VecTVJDT7.append( tJDdata.m_fGeothermal7 );
        m_VecTVJDT8.append( tJDdata.m_fGeothermal8 );
        m_VecTVJDT9.append( tJDdata.m_fGeothermal9 );
        m_VecTVJDT10.append( tJDdata.m_fGeothermal10 );
        m_VecTVGCS1.append( tGCdata.m_fstrain1 );
        m_VecTVGCS2.append( tGCdata.m_fstrain2 );
        m_VecTVGCS3.append( tGCdata.m_fstrain3 );
        m_VecTVGCS4.append( tGCdata.m_fstrain4 );

        m_VecTVGCS5.append( tGCdata.m_fstrain5 );
        m_VecTVGCS6.append( tGCdata.m_fstrain6 );
        m_VecTVGCS7.append( tGCdata.m_fstrain7 );
        m_VecTVGCS8.append( tGCdata.m_fstrain8 );
        m_VecTVGCS9.append( tGCdata.m_fstrain9 );
        m_VecTVGCS10.append( tGCdata.m_fstrain10 );

        m_VecTVGCT1.append( tGCdata.m_fGeothermal1 );
        m_VecTVGCT2.append( tGCdata.m_fGeothermal2 );
        m_VecTVGCT3.append( tGCdata.m_fGeothermal3 );
        m_VecTVGCT4.append( tGCdata.m_fGeothermal4 );
        m_VecTVGCT5.append( tGCdata.m_fGeothermal5 );
        m_VecTVGCT6.append( tGCdata.m_fGeothermal6 );
        m_VecTVGCT7.append( tGCdata.m_fGeothermal7 );
        m_VecTVGCT8.append( tGCdata.m_fGeothermal8 );
        m_VecTVGCT9.append( tGCdata.m_fGeothermal9 );
        m_VecTVGCT10.append( tGCdata.m_fGeothermal10 );
        m_VecTVBHX.append( tIMU.m_fRouteX );
        m_VecTVBHY.append( tIMU.m_fRouteY );
        m_VecTVBHZ.append( tIMU.m_fRouteZ );

    }
}

void HistoryData::GetMaxMin(QVector<double> vec1, QVector<double> vec2, QVector<double> vec3, QVector<double> vec4, QVector<double> vec5, double &dMax, double &dMin)
{
    QVector<double> dVecMin;
    QVector<double> dVecMax;
    auto max1 = std::max_element(std::begin(vec1), std::end(vec1));
    //最小值表示：
    auto min1 = std::min_element(std::begin(vec1), std::end(vec1));
    dVecMin.append( *min1 );
    dVecMax.append( *max1 );
    auto max2 = std::max_element(std::begin(vec2), std::end(vec2));
    //最小值表示：
    auto min2 = std::min_element(std::begin(vec2), std::end(vec2));

    dVecMin.append( *min2 );
    dVecMax.append( *max2 );

    auto max3 = std::max_element(std::begin(vec3), std::end(vec3));
    //最小值表示：
    auto min3 = std::min_element(std::begin(vec3), std::end(vec3));

    dVecMin.append( *min3 );
    dVecMax.append( *max3 );

    auto max4 = std::max_element(std::begin(vec4), std::end(vec4));
    //最小值表示：
    auto min4 = std::min_element(std::begin(vec4), std::end(vec4));

    dVecMin.append( *min4 );
    dVecMax.append( *max4 );

    auto max5 = std::max_element(std::begin(vec5), std::end(vec5));
    //最小值表示：
    auto min5 = std::min_element(std::begin(vec5), std::end(vec5));

    dVecMin.append( *min5 );
    dVecMax.append( *max5 );

    auto tMax = std::max_element(std::begin(dVecMax), std::end(dVecMax));
    //最小值表示：
    auto tMin = std::min_element(std::begin(dVecMin), std::end(dVecMin));

    dMax = *tMax;
    dMin = *tMin;
}

void HistoryData::Slot_timer()
{
    double dMin = 0; double dMax = 0;
    if( ( ++m_Index ) >= m_tagT.size()  )
    {
        QMessageBox::critical(0, QObject::tr("回放状态"),"回放结束", QMessageBox::Cancel);
        m_Index = -1;
        m_tagGC.clear();
        m_tagJD.clear();
        m_tagBH.clear();
        m_tagT.clear();
        m_Timer.stop();
        return;

    }
    //更新图表数据
    UpdateDataJGP( m_tagGC.at( m_Index ) , m_tagJD.at( m_Index ) , m_tagBH.at( m_Index ) );

    ui->JDHGroTempature1->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVJDT1 , m_VecTVJDT2  , m_VecTVJDT3 , m_VecTVJDT4 , m_VecTVJDT5 ,  dMax , dMin );
    double dRange = 0.50 * (dMax-dMin);
    double dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1 ){
        ui->JDHGroTempature1->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->JDHGroTempature1->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->JDHGroTempature1->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->JDHGroTempature1->graph(0)->setData( m_vecT , m_VecTVJDT1 );
    ui->JDHGroTempature1->graph(1)->setData( m_vecT , m_VecTVJDT2 );
    ui->JDHGroTempature1->graph(2)->setData( m_vecT , m_VecTVJDT3 );
    ui->JDHGroTempature1->graph(3)->setData( m_vecT , m_VecTVJDT4 );
    ui->JDHGroTempature1->graph(4)->setData( m_vecT , m_VecTVJDT5 );
    ui->JDHGroTempature1->replot();

    ui->JDHGroTempature2->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVJDT6 ,  m_VecTVJDT7 , m_VecTVJDT8 , m_VecTVJDT9 , m_VecTVJDT10 , dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->JDHGroTempature2->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->JDHGroTempature2->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->JDHGroTempature2->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->JDHGroTempature2->graph(0)->setData( m_vecT , m_VecTVJDT6 );
    ui->JDHGroTempature2->graph(1)->setData( m_vecT , m_VecTVJDT7 );
    ui->JDHGroTempature2->graph(2)->setData( m_vecT , m_VecTVJDT8 );
    ui->JDHGroTempature2->graph(3)->setData( m_vecT , m_VecTVJDT9 );
    ui->JDHGroTempature2->graph(4)->setData( m_vecT , m_VecTVJDT10 );
    ui->JDHGroTempature2->replot();

    ui->JDHStrain1->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVJDS1 , m_VecTVJDS2 , m_VecTVJDS3 ,m_VecTVJDS4 , m_VecTVJDS5 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->JDHStrain1->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->JDHStrain1->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->JDHStrain1->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->JDHStrain1->graph(0)->setData( m_vecT , m_VecTVJDS1 );
    ui->JDHStrain1->graph(1)->setData( m_vecT , m_VecTVJDS2 );
    ui->JDHStrain1->graph(2)->setData( m_vecT , m_VecTVJDS3 );
    ui->JDHStrain1->graph(3)->setData( m_vecT , m_VecTVJDS4 );
    ui->JDHStrain1->graph(4)->setData( m_vecT , m_VecTVJDS5 );
    ui->JDHStrain1->replot();

    ui->JDHStrain2->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVJDS6 , m_VecTVJDS7 , m_VecTVJDS8 ,m_VecTVJDS9 , m_VecTVJDS10 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->JDHStrain2->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->JDHStrain2->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->JDHStrain2->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->JDHStrain2->graph(0)->setData( m_vecT , m_VecTVJDS6 );
    ui->JDHStrain2->graph(1)->setData( m_vecT , m_VecTVJDS7 );
    ui->JDHStrain2->graph(2)->setData( m_vecT , m_VecTVJDS8 );
    ui->JDHStrain2->graph(3)->setData( m_vecT , m_VecTVJDS9 );
    ui->JDHStrain2->graph(4)->setData( m_vecT , m_VecTVJDS10 );
    ui->JDHStrain2->replot();

    ui->HGCGroTempature1->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 , m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVGCT1 , m_VecTVGCT2 , m_VecTVGCT3 ,m_VecTVGCT4 , m_VecTVGCT5 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->HGCGroTempature1->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->HGCGroTempature1->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->HGCGroTempature1->yAxis->setRange( dMin -iStep , dMax+ iStep );
    ui->HGCGroTempature1->graph(0)->setData( m_vecT , m_VecTVGCT1 );
    ui->HGCGroTempature1->graph(1)->setData( m_vecT , m_VecTVGCT2 );
    ui->HGCGroTempature1->graph(2)->setData( m_vecT , m_VecTVGCT3 );
    ui->HGCGroTempature1->graph(3)->setData( m_vecT , m_VecTVGCT4 );
    ui->HGCGroTempature1->graph(4)->setData( m_vecT , m_VecTVGCT5 );
    ui->HGCGroTempature1->replot();

    ui->HGCGroTempature2->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVGCT6 , m_VecTVGCT7 , m_VecTVGCT8 ,m_VecTVGCT9 , m_VecTVGCT10 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->HGCGroTempature2->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->HGCGroTempature2->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->HGCGroTempature2->yAxis->setRange( dMin -iStep , dMax+ iStep );

    ui->HGCGroTempature2->graph(0)->setData( m_vecT , m_VecTVGCT6 );
    ui->HGCGroTempature2->graph(1)->setData( m_vecT , m_VecTVGCT7 );
    ui->HGCGroTempature2->graph(2)->setData( m_vecT , m_VecTVGCT8 );
    ui->HGCGroTempature2->graph(3)->setData( m_vecT , m_VecTVGCT9 );
    ui->HGCGroTempature2->graph(4)->setData( m_vecT , m_VecTVGCT10 );
    ui->HGCGroTempature2->replot();

    ui->HGCStrain1->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVGCS1 , m_VecTVGCS2 , m_VecTVGCS3 ,m_VecTVGCS4 , m_VecTVGCS5 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->HGCStrain1->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->HGCStrain1->yAxis->setRange( dAvg - dRange*1.2, dAvg + dRange*1.2 );
    }
    //ui->HGCStrain1->yAxis->setRange( dMin -iStep , dMax+ iStep );

    ui->HGCStrain1->graph(0)->setData( m_vecT , m_VecTVGCS1 );
    ui->HGCStrain1->graph(1)->setData( m_vecT , m_VecTVGCS2 );
    ui->HGCStrain1->graph(2)->setData( m_vecT , m_VecTVGCS3 );
    ui->HGCStrain1->graph(3)->setData( m_vecT , m_VecTVGCS4 );
    ui->HGCStrain1->graph(4)->setData( m_vecT , m_VecTVGCS5 );
    ui->HGCStrain1->replot();

    ui->HGCStrain2->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 , m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    GetMaxMin( m_VecTVGCS6 , m_VecTVGCS7 , m_VecTVGCS8 ,m_VecTVGCS9 , m_VecTVGCS10 ,   dMax , dMin );
    dRange = 0.50 * (dMax-dMin);
    dAvg = 0.5 * (dMax+dMin);
    if(dRange < 1  ){
        ui->HGCStrain2->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->HGCStrain2->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->HGCStrain2->yAxis->setRange( dMin -10 , dMax+ 10 );
    ui->HGCStrain2->graph(0)->setData( m_vecT , m_VecTVGCS6 );
    ui->HGCStrain2->graph(1)->setData( m_vecT , m_VecTVGCS7 );
    ui->HGCStrain2->graph(2)->setData( m_vecT , m_VecTVGCS8 );
    ui->HGCStrain2->graph(3)->setData( m_vecT , m_VecTVGCS9 );
    ui->HGCStrain2->graph(4)->setData( m_vecT , m_VecTVGCS10 );

    ui->HGCStrain2->replot();

    ui->HRoteX->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    auto maxX = std::max_element(std::begin(m_VecTVBHX), std::end(m_VecTVBHX));
    //最小值表示：
    auto minX = std::min_element(std::begin(m_VecTVBHX), std::end(m_VecTVBHX));
    dRange = 0.50 * (*maxX - *minX);
    dAvg = 0.5 * (*maxX + *minX);
    if(dRange<1){
        ui->HRoteX->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->HRoteX->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->HRoteX->yAxis->setRange( *minX -iStep , *maxX+ iStep );
    ui->HRoteX->graph(0)->setData( m_vecT , m_VecTVBHX );
    ui->HRoteX->replot();

    ui->HRoteY->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    auto maxY = std::max_element(std::begin(m_VecTVBHY), std::end(m_VecTVBHY));
    //最小值表示：
    auto minY = std::min_element(std::begin(m_VecTVBHY), std::end(m_VecTVBHY));
    dRange = 0.50 * (*maxY - *minY);
    dAvg = 0.5 * (*maxY + *minY);
    if(dRange<1){
        ui->HRoteY->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->HRoteY->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->HRoteY->yAxis->setRange( *minY -iStep , *maxY+ iStep );
    ui->HRoteY->graph(0)->setData( m_vecT , m_VecTVBHY );
    ui->HRoteY->replot();

    ui->HRoteZ->xAxis->setRange( m_tagT.at(m_Index).toSecsSinceEpoch() - 100 ,  m_tagT.at(m_Index).toSecsSinceEpoch() + 10 );
    auto maxZ = std::max_element(std::begin(m_VecTVBHZ), std::end(m_VecTVBHZ));
    //最小值表示：
    auto minZ = std::min_element(std::begin(m_VecTVBHZ), std::end(m_VecTVBHZ));
    dRange = 0.50 * (*maxZ - *minZ);
    dAvg = 0.5 * (*maxZ + *minZ);
    if(dRange<1 ){
        ui->HRoteZ->yAxis->setRange( dAvg - iStep*0.6 , dAvg + iStep*0.6 );
    }
    else{
        ui->HRoteZ->yAxis->setRange( dAvg - dRange*1.2 , dAvg + dRange*1.2 );
    }
    //ui->HRoteZ->yAxis->setRange( *minZ -iStep , *maxZ+ iStep );
    ui->HRoteZ->graph(0)->setData( m_vecT , m_VecTVBHZ );
    ui->HRoteZ->replot();
}

void HistoryData::on_PBResearch_clicked()
{
    QDateTime tDate1 = ui->dateTimeEdit->dateTime();
    QDateTime tDate2 = ui->dateTimeEdit_2->dateTime();

    m_Index = -1;
    m_tagGC.clear();
    m_tagJD.clear();
    m_tagBH.clear();
    m_tagT.clear();
    m_Timer.stop();
    if( tDate1 > tDate2 )
    {
        QMessageBox::critical(0, QObject::tr("时间格式"),"起始时间必须大于结束时间...", QMessageBox::Cancel);
        return;
    }
    emit SIGNAL_OUT_Date( tDate1 , tDate2 );
    ui->PBResearch->setEnabled( false );
    ui->PBPlay->setEnabled( true );
    m_Timer.start();

}

void HistoryData::on_PBPlay_clicked()
{


    ui->PBResearch->setEnabled( true );
    ui->PBPlay->setEnabled( false );
    m_Timer.stop();
}


