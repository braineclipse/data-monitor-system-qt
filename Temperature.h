#ifndef TEMPERATURE_H
#define TEMPERATURE_H
#include<QSerialPort>
#include<QtSerialPort/QSerialPort>
#include <QObject>
#include "base_define.h"
class Temperature : public QObject
{
    Q_OBJECT
public:
    explicit Temperature(QObject *parent = nullptr);
    /**
     * @brief InitParam   初始化参数
     * @param strCom        串口号
     * @param iBaud       波特率
     */
    void InitParam( const QString& strCom , const int& iBaud  );
    /**
     * @brief ParamTemperature 解析温度数据
     * @param byte              温度数据报文
     */
    void ParamTemperature( QByteArray byte );
signals:
    /**
     * @brief TempValueChange 工程数据信号
     */
    void TempValueChange( tagData );

public slots:
    /**
     * @brief ReceiveDataSlot    接收串口数据槽函数
     */
    void ReceiveDataSlot();

    /**
     * @brief ThreadStartInit    线程启动初始化
     */
    void ThreadStartInit();
private:
    QSerialPort                 m_SerialPort;
    QString                     m_strCom;
    int                         m_iBaud;
    QByteArray                  m_ReceiveBytes;
    int                         m_iK;
};

#endif // TEMPERATURE_H
