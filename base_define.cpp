#include "base_define.h"
#include <string.h>
#include <math.h>

// 构造函数
tagData::tagData()
{
    memset( this, 0, sizeof( tagData ));
}

// 拷贝构造函数
tagData::tagData(const tagData &data)
{
    memcpy( this, &data, sizeof( tagData ) );
}

// 重载等号赋值
tagData tagData::operator=(const tagData &data)
{
    memcpy( this, &data, sizeof( tagData ) );
    return *this;
}
// 构造函数
tagIMU::tagIMU()
{
    memset( this, 0, sizeof( tagIMU ));
}

// 拷贝构造函数
tagIMU::tagIMU(const tagIMU &data)
{
    memcpy( this, &data, sizeof( tagIMU ) );
}

// 重载等号赋值
tagIMU tagIMU::operator=(const tagIMU &data)
{
    memcpy( this, &data, sizeof( tagIMU ) );
    return *this;
}
