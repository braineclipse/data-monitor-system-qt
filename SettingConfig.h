/**
  *                             Linux平台配置文件操作类
  *                 autor       ：         zj
  *                 date        ：         2018-08-06
  *                 node        ：         封装了配置文件基本操作
  *
  */
/*=============================FILE BEGIN ============================*/

#ifndef SETTINGCONFIG_H
#define SETTINGCONFIG_H
#include<QString>
#include <QSettings>
class SettingConfig
{
public:
    SettingConfig();
    ~SettingConfig();
    /**
      *向配置文件中写参数
      */
    void WriteSetting(const QString &groupname , const QString &key , const QString &value);
    /**
      *从配置文件中删除文件组
      */
    void deleteSetting();

    /**
      *从配置文件中写读参数
      */
    QString  ReadSetting ( QString groupname ,QString key ) ;
    /**
      *QSting转const char
      */
    char* ToConstChar(QString str) ;
    /**
      *QSting转 char
      */
    char  ToChar(QString str) ;
    /**
      *单选按钮写入配置文件
      */
    void CheckedState(int num ,Qt::CheckState& state) ;
    /**
      *初始化配置文件
      */
    void InitConfig(const QString& confName="Config.ini" );


private:

    QString                 m_strFileName;
    QSettings               *m_pSettings;

};

#endif // SETTINGCONFIG_H
/*=============================FILE END ============================*/
