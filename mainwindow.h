#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include"base_define.h"
#include <QMainWindow>
#include<QStringList>
#include<QVector>
#include "historydata.h"
#include"qcustomplot.h"
#include"Rotating.h"
#include"StrainData.h"
#include"Temperature.h"
#include"SettingConfig.h"
#include "sqlprogess.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    /**
    * @brief SoftWareTime  统计软件运行时间
    * @return
    */
   QString  SoftWareTime();
   /**
    * @brief InitModel  初始化解析函数
    */
   void InitModel();
   /**
    * @brief InitPlot   初始化plot窗口
    * @param plot        窗口指针
    */
   void InitPlot(QCustomPlot* plot  , int offset);

   /**
    * @brief InitPlotT   初始化地温plot窗口
    * @param plot        窗口指针
    */
   void InitPlotT(QCustomPlot* plot  , int offset);
   /**
    * @brief InitPlotR  初始化北航旋转数据
    * @param plotx      X轴旋转窗口
    * @param plotY      Y轴旋转窗口
    * @param plotZ      Z轴旋转窗口
    */
   void InitPlotR( QCustomPlot *plotX ,QCustomPlot *plotY ,QCustomPlot *plotZ  );

   /**
    * @brief SetLightColor  设置通信状态
    * @param lb             通信状态lable地址
    * @param bState         状态值，true 为绿色 ，  false 为红色
    */
   void SetLightColor(QLabel* lb , bool bState);

   void SetData();

public slots:
    void   slot_ShowHisWind();      //显示历史查询窗口
    void   slot_Timer();            //定时器槽函数，处理系统时间
    /**
     * @brief slot_ReceiveStrainSlot
     * @param data
     */
    void slot_ReceiveJD( tagData data );

    /**
     * @brief slot_ReceiveRotateSlot 接收北航旋转数据
     * @param IMUData
     */
    void slot_ReceiveRotateSlot( tagIMU IMUData );

    /**
     * @brief slot_ReceiveGC 接收GC数据
     * @param data
     */
    void slot_ReceiveGC( tagData data  );

    void UpdateDataJG( tagData tGCdata ,  tagData tJDdata , tagIMU tIMU );

    void UpdateBH( tagIMU tIMU );

    void GetMaxMin( QVector<double> vec1 , QVector<double> vec2 ,QVector<double> vec3, QVector<double> vec4 ,QVector<double> vec5,  double &dMax, double &dMin );


    void ProHisDate( QDateTime tDateS , QDateTime tDateT );
private:
    Ui::MainWindow *ui;
    HistoryData         *m_winHistoryData ; //历史数据查询窗口
    QTimer              m_Timer;
    qint64              m_DateTime;
    QStringList         m_strListLog;


    StrainData          m_StrainData;     //解析交大数据类
    QThread             m_StrainThread;

    Rotating            m_Rotating;       //解析北航数据类
    QThread             m_RotateThread;

    Temperature         m_Temperature;      //解析工程数据类
    QThread             m_TempThread;
    SettingConfig       m_SettingConf;
    SqlProgess          m_sql;
    bool                m_bSqlState;
    bool                bState[3] ;

    int                 iStep;

    tagData             m_tagJD;           //交大实时数据
    tagData             m_tagGC;            //工程实时数据
    tagIMU              m_tagBH;            //北航实时数据

    int                 m_iGrahpCount ;    //图表显示数据个数
    QVector<double>       m_vecT;         //时间轴数据
    QVector<double>      m_VecTVJDS1 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS2 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS3 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS4 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS5 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS6 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS7 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS8 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS9 ; //交大纵轴数据
    QVector<double>      m_VecTVJDS10 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT1 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT2 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT3 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT4 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT5 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT6 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT7 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT8 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT9 ; //交大纵轴数据
    QVector<double>      m_VecTVJDT10 ; //交大纵轴数据

    QVector<double>      m_VecTVGCS1 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS2 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS3 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS4 ; //工程纵轴数据


    QVector<double>      m_VecTVGCS5 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS6 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS7 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS8 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS9 ; //工程纵轴数据
    QVector<double>      m_VecTVGCS10 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT1 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT2 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT3 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT4 ; //工程纵轴数据


    QVector<double>      m_VecTVGCT5 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT6 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT7 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT8 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT9 ; //工程纵轴数据
    QVector<double>      m_VecTVGCT10 ; //工程纵轴数据
    QVector<double>      m_VecTVBHX ; //北航纵轴数据
    QVector<double>      m_VecTVBHY ; //北航纵轴数据
    QVector<double>      m_VecTVBHZ ; //北航纵轴数据

    QPen qPenSlot[5];
};
#endif // MAINWINDOW_H
