#include "DataBlockParser.h"
#include <memory.h>
#include <stdlib.h>

DataBlockParser::DataBlockParser() {
	pchannelBlock = new ChannelBlockData[20]();
	externData = new unsigned char[100]();
}
bool DataBlockParser::Parser(const unsigned char* pBlock, int blockSize, bool isLittleEndian) {
    if(blockSize < 214){
        return false;
    }
    bool res = true;
	frameHeader = *(int*)pBlock;
	timeStamp = *(int*)(pBlock + 4);
	for (int i = 0; i < 20; i++) {
		unsigned char subBlock[5] = { 0, };
		//把数据做一次拷贝
		memcpy_s(&subBlock, 5, pBlock + 8 + 5 * i, 5);
		//转换端序
        if (isLittleEndian) {
			for (int j = 0; j < 2; j++) {
				unsigned char temp = subBlock[j];
				subBlock[j] = subBlock[4 - j];
				subBlock[4 - j] = temp;
			}
		}
		//取位读数
		pchannelBlock[i].startHeader = subBlock[0] & 0B10000000;
		pchannelBlock[i].isTemperature = subBlock[0] & 0B01000000;
		pchannelBlock[i].numChannel &= (subBlock[0] & 0B00111100) >> 2;
		unsigned int senserData = 0x00000000;
		senserData |= (subBlock[0] & 0B00000011) << 30;
		senserData |= (subBlock[1]) << 22;
		senserData |= (subBlock[2]) << 14;
		senserData |= (subBlock[3]) << 6;
		senserData |= (subBlock[4] & 0B11111100) >> 2;
		pchannelBlock[i].channelData = (int)senserData;
		pchannelBlock[i].checkSum = subBlock[4] & 0B00000010;
		pchannelBlock[i].endTail = subBlock[4] & 0B00000001;
		//检查checkSum
		bool checkSumFlag = 0;
		checkSumFlag ^= pchannelBlock[i].isTemperature & 0x01;
		for (int checkSumLoop = 0; checkSumLoop < 4; checkSumLoop++) {
			checkSumFlag ^= (pchannelBlock[i].numChannel >> checkSumLoop) & 0x01;
		}
		for (int checkSumLoop = 0; checkSumLoop < 32; checkSumLoop++) {
			checkSumFlag ^= (pchannelBlock[i].channelData >> checkSumLoop) & 0x01;
		}
		if (checkSumFlag != pchannelBlock[i].checkSum) {
			res = false;
		}
	}
	//附加数据
	memcpy_s(externData, 100, pBlock + 108, 100);
	CRC16CheckSum = *(unsigned short*)(pBlock + 208);
	frameTail = *(int*)(pBlock + 210);
	return res;
}
bool DataBlockParser::GetChannelINFO(int blockIndex, unsigned char* const channel, int* const channelData,bool* isTemp) {
	if (pchannelBlock == nullptr || blockIndex >= 20) {
		return false;
	}
	*channel = pchannelBlock[blockIndex].numChannel;
	*channelData = pchannelBlock[blockIndex].channelData;
    *isTemp = pchannelBlock[blockIndex].isTemperature;
	return true;
}
int DataBlockParser::GetFrameHeader() {
	return frameHeader;
}
int DataBlockParser::GetTimeStamp() {
	return timeStamp;
}
bool DataBlockParser::GetExternData(unsigned char* const dstBlock, int copySize = 100) {
	if (externData == nullptr) {
		return false;
	}
	memcpy_s(dstBlock, copySize, externData, copySize);
	return true;
}
int DataBlockParser::GetFrameTail() {
	return frameTail;
}
bool DataBlockParser::GetBlockStruct(int blockIndex, const PChannelBlockData blockpointer){
    if(pchannelBlock == nullptr || blockIndex >= 20){
        return false;
    }
    blockpointer->endTail = pchannelBlock[blockIndex].endTail;
    blockpointer->checkSum = pchannelBlock[blockIndex].checkSum;
    blockpointer->numChannel = pchannelBlock[blockIndex].numChannel;
    blockpointer->channelData = pchannelBlock[blockIndex].channelData;
    blockpointer->startHeader = pchannelBlock[blockIndex].startHeader;
    blockpointer->isTemperature = pchannelBlock[blockIndex].isTemperature;
    return true;
}
DataBlockParser::~DataBlockParser() {
	if (pchannelBlock != nullptr) {
		free(pchannelBlock);
	}
	if (externData != nullptr) {
		free(externData);
	}
}
