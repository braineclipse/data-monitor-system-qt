#ifndef BASE_DEFINE_H
#define BASE_DEFINE_H

/**
  *                  基础定义模块，定义通用/共用宏、结构体、枚举、函数等
  *
  *                                 author : zj
  *                                   date : 2021.5.9
  */

#pragma pack(1)         // 取消4字节对齐

/**
  * 数据参数
  */
struct tagData
{
    unsigned  int               m_uiTime;                     //时间戳
    char                        m_cState1;
    float                       m_fGeothermal1;               //地温1
    char                        m_cState2;
    float                       m_fGeothermal2;                 // 地温2
    char                        m_cState3;
    float                       m_fGeothermal3;                 // 地温3
    char                        m_cState4;
    float                       m_fGeothermal4;                 // 地温4
    char                        m_cState5;
    float                       m_fGeothermal5;                 // 地温5
    char                        m_cState6;
    float                       m_fGeothermal6;                 // 地温6
    char                        m_cState7;
    float                       m_fGeothermal7;                 // 地温7
    char                        m_cState8;
    float                       m_fGeothermal8;                 // 地温8
    char                        m_cState9;
    float                       m_fGeothermal9;                 // 地温9
    char                        m_cState10;
    float                       m_fGeothermal10;               // 地温10
    char                        m_cSstate1;
    float                       m_fstrain1;                    // 应变1
    char                        m_cSstate2;
    float                       m_fstrain2;                    // 应变2
    char                        m_cSstate3;
    float                       m_fstrain3;                    // 应变3
    char                        m_cSstate4;
    float                       m_fstrain4;                    // 应变4
    char                        m_cSstate5;
    float                       m_fstrain5;                    // 应变5

    char                        m_cSstate6;
    float                       m_fstrain6;                    // 应变6
    char                        m_cSstate7;
    float                       m_fstrain7;                    // 应变7
    char                        m_cSstate8;
    float                       m_fstrain8;                    // 应变8
    char                        m_cSstate9;
    float                       m_fstrain9;                     // 应变9
    char                        m_cSstate10;
    float                       m_fstrain10;                   // 应变10

    tagData();

    // 拷贝构造函数
    tagData( const tagData& data );

    // 重载等号赋值
    tagData operator= ( const tagData& data );
};

struct tagIMU
{
    unsigned  int               m_uiTime;                     //时间戳
    char                        m_cStateX;
    float                       m_fRouteX;               //旋转X
    char                        m_cStateY;
    float                       m_fRouteY;                 // 旋转Y
    char                        m_cStateZ;
    float                       m_fRouteZ;                 // 旋转Z
    tagIMU();

    // 拷贝构造函数
    tagIMU( const tagIMU& data );

    // 重载等号赋值
    tagIMU operator= ( const tagIMU& data );
};

#pragma pack()

#endif // BASE_DEFINE_H
