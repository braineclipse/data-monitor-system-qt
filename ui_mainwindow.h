/********************************************************************************
** Form generated from reading UI file 'mainwindowurwYaI.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAINWINDOWURWYAI_H
#define MAINWINDOWURWYAI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout_10;
    QGridLayout *gridLayout_9;
    QTabWidget *tabData;
    QWidget *tabJD;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QCustomPlot *JDGroTempature1;
    QCustomPlot *JDGroTempature2;
    QHBoxLayout *horizontalLayout_2;
    QCustomPlot *GCGroTempature1;
    QCustomPlot *GCGroTempature2;
    QWidget *tabGC;
    QGridLayout *gridLayout_6;
    QHBoxLayout *horizontalLayout_3;
    QCustomPlot *GCStrain1;
    QCustomPlot *GCStrain2;
    QHBoxLayout *horizontalLayout_4;
    QCustomPlot *JDStrain1;
    QCustomPlot *JDStrain2;
    QWidget *tabBH;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QCustomPlot *RoteX;
    QCustomPlot *RoteY;
    QCustomPlot *RoteZ;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QPlainTextEdit *PTX;
    QHBoxLayout *horizontalLayout_10;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_7;
    QLabel *LBTime;
    QPushButton *PBHistory;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_8;
    QHBoxLayout *horizontalLayout_9;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_2;
    QLabel *LBCom1;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_4;
    QLabel *LBCom2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_6;
    QLabel *LBCom3;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1248, 741);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(14);
        MainWindow->setFont(font);
        MainWindow->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout_10 = new QGridLayout(centralwidget);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        gridLayout_9 = new QGridLayout();
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        tabData = new QTabWidget(centralwidget);
        tabData->setObjectName(QString::fromUtf8("tabData"));
        tabData->setMinimumSize(QSize(812, 0));
        tabData->setFont(font);
        tabData->setTabPosition(QTabWidget::East);
        tabJD = new QWidget();
        tabJD->setObjectName(QString::fromUtf8("tabJD"));
        gridLayout_5 = new QGridLayout(tabJD);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        JDGroTempature1 = new QCustomPlot(tabJD);
        JDGroTempature1->setObjectName(QString::fromUtf8("JDGroTempature1"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(JDGroTempature1->sizePolicy().hasHeightForWidth());
        JDGroTempature1->setSizePolicy(sizePolicy1);
        JDGroTempature1->setMinimumSize(QSize(300, 300));

        horizontalLayout->addWidget(JDGroTempature1);

        JDGroTempature2 = new QCustomPlot(tabJD);
        JDGroTempature2->setObjectName(QString::fromUtf8("JDGroTempature2"));
        sizePolicy1.setHeightForWidth(JDGroTempature2->sizePolicy().hasHeightForWidth());
        JDGroTempature2->setSizePolicy(sizePolicy1);
        JDGroTempature2->setMinimumSize(QSize(300, 300));

        horizontalLayout->addWidget(JDGroTempature2);


        gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        GCGroTempature1 = new QCustomPlot(tabJD);
        GCGroTempature1->setObjectName(QString::fromUtf8("GCGroTempature1"));
        sizePolicy1.setHeightForWidth(GCGroTempature1->sizePolicy().hasHeightForWidth());
        GCGroTempature1->setSizePolicy(sizePolicy1);
        GCGroTempature1->setMinimumSize(QSize(300, 300));

        horizontalLayout_2->addWidget(GCGroTempature1);

        GCGroTempature2 = new QCustomPlot(tabJD);
        GCGroTempature2->setObjectName(QString::fromUtf8("GCGroTempature2"));
        sizePolicy1.setHeightForWidth(GCGroTempature2->sizePolicy().hasHeightForWidth());
        GCGroTempature2->setSizePolicy(sizePolicy1);
        GCGroTempature2->setMinimumSize(QSize(300, 300));

        horizontalLayout_2->addWidget(GCGroTempature2);


        gridLayout_2->addLayout(horizontalLayout_2, 1, 0, 1, 1);


        gridLayout_5->addLayout(gridLayout_2, 0, 0, 1, 1);

        tabData->addTab(tabJD, QString());
        tabGC = new QWidget();
        tabGC->setObjectName(QString::fromUtf8("tabGC"));
        sizePolicy1.setHeightForWidth(tabGC->sizePolicy().hasHeightForWidth());
        tabGC->setSizePolicy(sizePolicy1);
        gridLayout_6 = new QGridLayout(tabGC);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        GCStrain1 = new QCustomPlot(tabGC);
        GCStrain1->setObjectName(QString::fromUtf8("GCStrain1"));
        sizePolicy1.setHeightForWidth(GCStrain1->sizePolicy().hasHeightForWidth());
        GCStrain1->setSizePolicy(sizePolicy1);
        GCStrain1->setMinimumSize(QSize(300, 300));

        horizontalLayout_3->addWidget(GCStrain1);

        GCStrain2 = new QCustomPlot(tabGC);
        GCStrain2->setObjectName(QString::fromUtf8("GCStrain2"));
        sizePolicy1.setHeightForWidth(GCStrain2->sizePolicy().hasHeightForWidth());
        GCStrain2->setSizePolicy(sizePolicy1);
        GCStrain2->setMinimumSize(QSize(300, 300));

        horizontalLayout_3->addWidget(GCStrain2);


        gridLayout_6->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        JDStrain1 = new QCustomPlot(tabGC);
        JDStrain1->setObjectName(QString::fromUtf8("JDStrain1"));
        sizePolicy1.setHeightForWidth(JDStrain1->sizePolicy().hasHeightForWidth());
        JDStrain1->setSizePolicy(sizePolicy1);
        JDStrain1->setMinimumSize(QSize(300, 300));

        horizontalLayout_4->addWidget(JDStrain1);

        JDStrain2 = new QCustomPlot(tabGC);
        JDStrain2->setObjectName(QString::fromUtf8("JDStrain2"));
        sizePolicy1.setHeightForWidth(JDStrain2->sizePolicy().hasHeightForWidth());
        JDStrain2->setSizePolicy(sizePolicy1);
        JDStrain2->setMinimumSize(QSize(300, 300));

        horizontalLayout_4->addWidget(JDStrain2);


        gridLayout_6->addLayout(horizontalLayout_4, 1, 0, 1, 1);

        tabData->addTab(tabGC, QString());
        tabBH = new QWidget();
        tabBH->setObjectName(QString::fromUtf8("tabBH"));
        sizePolicy1.setHeightForWidth(tabBH->sizePolicy().hasHeightForWidth());
        tabBH->setSizePolicy(sizePolicy1);
        gridLayout_3 = new QGridLayout(tabBH);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        RoteX = new QCustomPlot(tabBH);
        RoteX->setObjectName(QString::fromUtf8("RoteX"));
        sizePolicy1.setHeightForWidth(RoteX->sizePolicy().hasHeightForWidth());
        RoteX->setSizePolicy(sizePolicy1);
        RoteX->setMinimumSize(QSize(300, 300));

        horizontalLayout_5->addWidget(RoteX);

        RoteY = new QCustomPlot(tabBH);
        RoteY->setObjectName(QString::fromUtf8("RoteY"));
        sizePolicy1.setHeightForWidth(RoteY->sizePolicy().hasHeightForWidth());
        RoteY->setSizePolicy(sizePolicy1);
        RoteY->setMinimumSize(QSize(300, 300));

        horizontalLayout_5->addWidget(RoteY);


        gridLayout_4->addLayout(horizontalLayout_5, 0, 0, 1, 1);

        RoteZ = new QCustomPlot(tabBH);
        RoteZ->setObjectName(QString::fromUtf8("RoteZ"));
        sizePolicy1.setHeightForWidth(RoteZ->sizePolicy().hasHeightForWidth());
        RoteZ->setSizePolicy(sizePolicy1);
        RoteZ->setMinimumSize(QSize(300, 300));

        gridLayout_4->addWidget(RoteZ, 1, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_4, 0, 0, 1, 1);

        tabData->addTab(tabBH, QString());

        gridLayout_9->addWidget(tabData, 0, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMaximumSize(QSize(408, 16777215));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        PTX = new QPlainTextEdit(groupBox);
        PTX->setObjectName(QString::fromUtf8("PTX"));
        PTX->setFont(font);
        PTX->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));

        gridLayout->addWidget(PTX, 0, 0, 1, 1);


        verticalLayout->addWidget(groupBox);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(316, 65));
        gridLayout_7 = new QGridLayout(groupBox_2);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        LBTime = new QLabel(groupBox_2);
        LBTime->setObjectName(QString::fromUtf8("LBTime"));
        QFont font1;
        font1.setPointSize(21);
        font1.setBold(true);
        font1.setWeight(75);
        LBTime->setFont(font1);
        LBTime->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(LBTime, 0, 0, 1, 1);


        horizontalLayout_10->addWidget(groupBox_2);

        PBHistory = new QPushButton(centralwidget);
        PBHistory->setObjectName(QString::fromUtf8("PBHistory"));
        PBHistory->setMinimumSize(QSize(0, 47));

        horizontalLayout_10->addWidget(PBHistory);


        verticalLayout->addLayout(horizontalLayout_10);

        groupBox_3 = new QGroupBox(centralwidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_8 = new QGridLayout(groupBox_3);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(label_2);

        LBCom1 = new QLabel(groupBox_3);
        LBCom1->setObjectName(QString::fromUtf8("LBCom1"));
        LBCom1->setMinimumSize(QSize(0, 43));
        QFont font2;
        font2.setPointSize(15);
        LBCom1->setFont(font2);
        LBCom1->setAlignment(Qt::AlignCenter);

        horizontalLayout_6->addWidget(LBCom1);


        horizontalLayout_9->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_4 = new QLabel(groupBox_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_4);

        LBCom2 = new QLabel(groupBox_3);
        LBCom2->setObjectName(QString::fromUtf8("LBCom2"));
        LBCom2->setFont(font2);
        LBCom2->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(LBCom2);


        horizontalLayout_9->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_6 = new QLabel(groupBox_3);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(label_6);

        LBCom3 = new QLabel(groupBox_3);
        LBCom3->setObjectName(QString::fromUtf8("LBCom3"));
        LBCom3->setFont(font2);
        LBCom3->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(LBCom3);


        horizontalLayout_9->addLayout(horizontalLayout_8);


        gridLayout_8->addLayout(horizontalLayout_9, 0, 0, 1, 1);


        verticalLayout->addWidget(groupBox_3);


        gridLayout_9->addLayout(verticalLayout, 0, 1, 1, 1);


        gridLayout_10->addLayout(gridLayout_9, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1248, 23));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        tabData->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "\345\234\260\351\234\207\347\233\221\346\216\247\347\263\273\347\273\237", nullptr));
        tabData->setTabText(tabData->indexOf(tabJD), QCoreApplication::translate("MainWindow", " \346\225\260\346\215\2561", nullptr));
        tabData->setTabText(tabData->indexOf(tabGC), QCoreApplication::translate("MainWindow", "\346\225\260\346\215\2562", nullptr));
        tabData->setTabText(tabData->indexOf(tabBH), QCoreApplication::translate("MainWindow", "\346\225\260\346\215\2563", nullptr));
        groupBox->setTitle(QCoreApplication::translate("MainWindow", "\346\227\245\345\277\227\344\277\241\346\201\257", nullptr));
        PTX->setPlainText(QCoreApplication::translate("MainWindow", "2019-12-24 13\357\274\23212\357\274\23211  \350\256\276\345\244\207\345\220\257\345\212\250......\n"
"2019-12-24 13\357\274\23213\357\274\23234  \347\263\273\347\273\237\347\237\253\346\227\266\346\210\220\345\212\237......\n"
"2019-12-24 13\357\274\23214\357\274\23252  \346\216\245\346\224\266GPS\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23215\357\274\23275  \346\216\245\346\224\266\345\272\224\345\217\230\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23216\357\274\23219  \346\216\245\346\224\266\345\234\260\346\270\251\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23217\357\274\23239  \346\216\245\346\224\266\346\227\213\350\275\254\345\210\206\351\207\217\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23218\357\274\23217  GPS\344\277\241\345\217\267\345\274\202\345\270\270......\n"
"2019-12-24 13\357\274\23212\357\274\23211  \350\256\276\345\244\207\345\220\257\345\212\250......\n"
"2019-12-24 13\357\274\23213\357\274\23234  \347\263\273\347\273\237\346\240\241\346"
                        "\227\266\346\210\220\345\212\237......\n"
"2019-12-24 13\357\274\23214\357\274\23252  \346\216\245\346\224\266GPS\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23215\357\274\23275  \346\216\245\346\224\266\345\272\224\345\217\230\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23216\357\274\23219  \346\216\245\346\224\266\345\234\260\346\270\251\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23217\357\274\23239  \346\216\245\346\224\266\346\227\213\350\275\254\345\210\206\351\207\217\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23218\357\274\23217  GPS\344\277\241\345\217\267\345\274\202\345\270\270......\n"
"2019-12-24 13\357\274\23212\357\274\23211  \350\256\276\345\244\207\345\220\257\345\212\250......\n"
"2019-12-24 13\357\274\23213\357\274\23234  \347\263\273\347\273\237\347\237\253\346\227\266\346\210\220\345\212\237......\n"
"2019-12-24 13\357\274\23214\357\274\23252  \346\216\245\346\224\266GPS\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23215\357\274\232"
                        "75  \346\216\245\346\224\266\345\272\224\345\217\230\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23216\357\274\23219  \346\216\245\346\224\266\345\234\260\346\270\251\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23217\357\274\23239  \346\216\245\346\224\266\346\227\213\350\275\254\345\210\206\351\207\217\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23218\357\274\23217  GPS\344\277\241\345\217\267\345\274\202\345\270\270......\n"
"2019-12-24 13\357\274\23212\357\274\23211  \350\256\276\345\244\207\345\220\257\345\212\250......\n"
"2019-12-24 13\357\274\23213\357\274\23234  \347\263\273\347\273\237\347\237\253\346\227\266\346\210\220\345\212\237......\n"
"2019-12-24 13\357\274\23214\357\274\23252  \346\216\245\346\224\266GPS\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23215\357\274\23275  \346\216\245\346\224\266\345\272\224\345\217\230\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23216\357\274\23219  \346\216\245\346\224\266\345\234\260\346\270\251\346\225"
                        "\260\346\215\256......\n"
"2019-12-24 13\357\274\23217\357\274\23239  \346\216\245\346\224\266\346\227\213\350\275\254\345\210\206\351\207\217\346\225\260\346\215\256......\n"
"2019-12-24 13\357\274\23218\357\274\23217  GPS\344\277\241\345\217\267\345\274\202\345\270\270......\n"
"2019-12-24 13\357\274\23218\357\274\23217  GPS\344\277\241\345\217\267\345\274\202\345\270\270......", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("MainWindow", "\347\263\273\347\273\237\346\227\266\351\227\264", nullptr));
        LBTime->setText(QCoreApplication::translate("MainWindow", "2021-12-21 14:13:24", nullptr));
        PBHistory->setText(QCoreApplication::translate("MainWindow", "\345\216\206\345\217\262\346\225\260\346\215\256", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("MainWindow", "  \351\200\232\344\277\241\347\212\266\346\200\201", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", " COM1\357\274\232", nullptr));
        LBCom1->setText(QString());
        label_4->setText(QCoreApplication::translate("MainWindow", " COM2\357\274\232", nullptr));
        LBCom2->setText(QString());
        label_6->setText(QCoreApplication::translate("MainWindow", " COM3\357\274\232", nullptr));
        LBCom3->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAINWINDOWURWYAI_H
