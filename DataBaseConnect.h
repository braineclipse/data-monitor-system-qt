/********************************************************************************
** Form generated from reading UI file 'DataBaseConnect.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef DATABASECONNECT_H
#define DATABASECONNECT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QDialogButtonBox *ui_PassInputCheck;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *ui_UserNameInputBar;
    QLineEdit *ui_PasswordBar;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(400, 174);
        ui_PassInputCheck = new QDialogButtonBox(Dialog);
        ui_PassInputCheck->setObjectName(QString::fromUtf8("ui_PassInputCheck"));
        ui_PassInputCheck->setGeometry(QRect(30, 130, 341, 32));
        ui_PassInputCheck->setOrientation(Qt::Horizontal);
        ui_PassInputCheck->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label = new QLabel(Dialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(30, 20, 54, 12));
        label_2 = new QLabel(Dialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(30, 60, 54, 12));
        ui_UserNameInputBar = new QLineEdit(Dialog);
        ui_UserNameInputBar->setObjectName(QString::fromUtf8("ui_UserNameInputBar"));
        ui_UserNameInputBar->setGeometry(QRect(100, 20, 261, 20));
        ui_PasswordBar = new QLineEdit(Dialog);
        ui_PasswordBar->setObjectName(QString::fromUtf8("ui_PasswordBar"));
        ui_PasswordBar->setGeometry(QRect(100, 60, 261, 20));
        ui_PasswordBar->setEchoMode(QLineEdit::Password);

        retranslateUi(Dialog);
        QObject::connect(ui_PassInputCheck, SIGNAL(accepted()), Dialog, SLOT(accept()));
        QObject::connect(ui_PassInputCheck, SIGNAL(rejected()), Dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QCoreApplication::translate("Dialog", "Login", nullptr));
        label->setText(QCoreApplication::translate("Dialog", "UserName", nullptr));
        label_2->setText(QCoreApplication::translate("Dialog", "Password", nullptr));
    } // retranslateUi

    void GetInputLoginInfo(QString * qStrUserInfo,QString * qStrPassInfo){
        if(qStrUserInfo != nullptr && qStrPassInfo != nullptr){
            qStrUserInfo->append(this->ui_UserNameInputBar->text());
            qStrPassInfo->append(this->ui_PasswordBar->text());
        }
    }
};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // DATABASECONNECT_H
