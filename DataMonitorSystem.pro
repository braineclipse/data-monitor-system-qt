QT       += core gui  serialport sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    DataBlockParser.cpp \
    Rotating.cpp \
    SettingConfig.cpp \
    StrainData.cpp \
    Temperature.cpp \
    base_define.cpp \
    historydata.cpp \
    main.cpp \
    mainwindow.cpp \
    qcustomplot.cpp \
    sqlprogess.cpp

HEADERS += \
    DataBaseConnect.h \
    DataBlockParser.h \
    Rotating.h \
    SettingConfig.h \
    StrainData.h \
    Temperature.h \
    base_define.h \
    historydata.h \
    mainwindow.h \
    qcustomplot.h \
    sqlprogess.h

FORMS += \
    DataBaseConnect.ui \
    historydata.ui \
    mainwindow.ui
RESOURCES += \
    rc.qrc \
    logo.qrc

OTHER_FILES += \
    myico.rc \
    favicon.ico
RC_FILE += myico.rc

DISTFILES += \
    myico.rc
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
