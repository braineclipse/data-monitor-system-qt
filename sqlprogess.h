#ifndef SQLPROGESS_H
#define SQLPROGESS_H
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QObject>
#include<QTextCodec>
#include<QString>
#include<QByteArray>
#include<QDateTime>
#include<QVariant>
#include<QDebug>
#include <QSqlError>
#include"base_define.h"
class SqlProgess
{

public:
    SqlProgess();
    ~SqlProgess();
    /**
     * @brief createConnect  创建数据库连接
     * @return
     */
    bool createConnect();

    /**
     * @brief InsertDataTest 测试数据库函数
     */
    void InsertDataTest();

    /**
     * @brief InsertData   向数据库写入数据
     * @param TableName    表
     * @param dJD           交大数据
     * @param dGC           工程数据
     * @param dBH           北航数据
     */
    void InsertData( QString  TableName , tagData dJD , tagData dGC , tagIMU dBH);

    /**
     * @brief CreateTable 创建数据表函数
     * @param strTableName  表名
     * @return
     */
    bool CreateTable( QString strTableName );
    /**
     * @brief ReadSearchData 指定日期查询数据
     * @param StartD        开始时间
     * @param endD          结束时间
     */

    void ReadSearchData( QString TableName ,QDateTime StartD , QDateTime endD );

    QVector<tagData> GetJD();
public:

    QVector<tagData> m_VecJD;    //存储交大查询数据
    QVector<tagData> m_VecGC;    // 存储工程查询数据
    QVector<tagIMU> m_VecBH;     // 存储北航查询数据
    QVector<QDateTime>    m_DateT;

private:

    QSqlDatabase     m_db;      //数据库实例对象


};

#endif // SQLPROGESS_H
