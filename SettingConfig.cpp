/**
  *                             Linux平台配置文件操作类
  *                 autor       ：         zj
  *                 date        ：         2018-08-06
  *                 node        ：         封装了配置文件基本操作
  *
  */
/*=============================FILE BEGIN ============================*/
#include "SettingConfig.h"

SettingConfig::SettingConfig()
{
    m_strFileName = "";
}

SettingConfig::~SettingConfig()
{
    delete m_pSettings;
}
/**
  *初始化配置文件
  */
void SettingConfig::InitConfig(const QString& confName )
{
    m_strFileName = confName;
    m_pSettings= new QSettings(m_strFileName, QSettings::IniFormat );// QSettings::IniFormat);
}
/**
 *向配置文件中写参数
 */
void SettingConfig::WriteSetting( const QString& groupname, const  QString& key, const QString &value)
{
    m_pSettings->beginGroup(groupname);
    m_pSettings->setValue(key, value);
    m_pSettings->endGroup();
}
/**
 *从配置文件中读参数
 */
QString SettingConfig::ReadSetting( QString groupname, QString key)
{
    QString value ;
    m_pSettings->beginGroup(groupname);
    value = m_pSettings->value(key).toString();
    m_pSettings->endGroup();
    return value ;
}
/**
 *从配置文件中删除文件组
 */
void SettingConfig::deleteSetting()
{
     m_pSettings->clear();
}
/**
 *QSting转const char
 */
char *SettingConfig::ToConstChar( QString str )
{
    return str.toLatin1().data() ;
}
/**
 *QSting转 char
 */
char SettingConfig::ToChar(QString str)
{
    return str.toInt() ;
}
/**
 *单选按钮写入配置文件
 */
void SettingConfig::CheckedState(int num, Qt::CheckState &state)
{
    if ( num == 0)
        state = Qt::Unchecked ;
    if ( num == 1 )
        state = Qt::PartiallyChecked ;
    if(num == 2)
        state = Qt::Checked ;
}
/*=============================FILE END ============================*/
